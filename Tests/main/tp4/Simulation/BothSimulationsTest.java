package main.tp4.Simulation;

import main.tp4.Simulation.Models.OptionA;
import main.tp4.Simulation.Models.OptionB;

public class BothSimulationsTest {

    public static void main(String[] args) {

        OptionA optionA = new OptionA();
        optionA.simulation();
        int clientsAttendedA = optionA.getClientsAttended();
        double timeOpenedAfterClosingA = optionA.getTimeOpenedAfterClosing() / 60;
        System.out.println("During the simulation of Option A, a total of " + clientsAttendedA + " clients were attended in a working day, \n" +
                            "and the Bank was open for " + timeOpenedAfterClosingA + " minutes after the official closing time, attending \n" +
                            "the remaining clients.");

        System.out.println();

        OptionB optionB = new OptionB();
        optionB.simulation();
        int clientsAttendedB = optionB.getClientsAttended();
        double timeOpenedAfterClosingB = optionB.getTimeOpenedAfterClosing() / 60;
        System.out.println("During the simulation of Option B, a total of " + clientsAttendedB + " clients were attended in a working day, \n" +
                            "and the Bank was open for " + timeOpenedAfterClosingB + " minutes after the official closing time, attending \n" +
                            "the remaining clients.");

        System.out.println();

        if (clientsAttendedA < clientsAttendedB) {
            System.out.println("Option B is the more efficient alternative");
        } else if (clientsAttendedA > clientsAttendedB) {
            System.out.println("Option A is the more efficient alternative");
        } else {
            System.out.println("Both options are equally efficient");
        }
    }
}
