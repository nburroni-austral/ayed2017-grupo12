package main.tp4.Palindrome;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PalindromeTest {

    @Test
    public void palindromeTest1() {

        Palindrome palindrome = new Palindrome("Neuquen");
        boolean test = palindrome.check();
        assertTrue(test);
    }

    @Test
    public void palindromeTest2() {

        Palindrome palindrome = new Palindrome("La ruta natural");
        boolean test = palindrome.check();
        assertTrue(test);
    }

    @Test
    public void palindromeTest3() {

        Palindrome palindrome = new Palindrome("Computadora");
        boolean test = palindrome.check();
        assertFalse(test);
    }

    @Test
    public void palindromeTest4() {

        Palindrome palindrome = new Palindrome("La ruta del desierto");
        boolean test = palindrome.check();
        assertFalse(test);
    }
}
