package main.tp4.PriorityQueue;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PriorityQueueTest {
    @Test
    @SuppressWarnings("unchecked")
    public void PriorityQueueTesting(){
        PriorityQueue pq = new PriorityQueue(5);
        pq.enqueue(1,1);
        pq.enqueue(2,2);
        pq.enqueue(3,3);
        pq.enqueue(4,4);
        pq.enqueue(5,5);

        assertEquals(1,pq.dequeue());
        assertEquals(2,pq.dequeue());
        assertEquals(3,pq.dequeue());
        assertEquals(4,pq.dequeue());
        assertEquals(5,pq.dequeue());
        assertEquals(null,pq.dequeue());

        pq.enqueue(1,1);
        pq.enqueue(2,1);
        assertEquals(1,pq.dequeue());
        assertEquals(2,pq.dequeue());
    }

}
