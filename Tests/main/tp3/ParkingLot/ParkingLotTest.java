package main.tp3.ParkingLot;

import org.junit.Test;
import static org.junit.Assert.*;

public class ParkingLotTest {

    @Test
    public void addCarToParkingLot() {
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        parkingLot.addCarToParkingLot(car1);
        parkingLot.addCarToParkingLot(car2);
        int parkingLotSize = parkingLot.getTheParkingLot().size();
        assertEquals(2, parkingLotSize);
    }

    @Test
    public void removeCar() {
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        parkingLot.addCarToParkingLot(car1);
        parkingLot.addCarToParkingLot(car2);
        parkingLot.removeCar(car1);
        int parkingLotSize = parkingLot.getTheParkingLot().size();
        assertEquals(1, parkingLotSize);
    }

    @Test
    public void getFare() {
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        parkingLot.addCarToParkingLot(car1);
        parkingLot.addCarToParkingLot(car2);
        parkingLot.addCarToParkingLot(car3);
        parkingLot.removeCar(car1);
        int fare = parkingLot.getFare();
        assertEquals(15, fare);
    }
}
