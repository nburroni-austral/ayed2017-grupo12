package main.tp3.Calculator;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by arimi on 28-Mar-17.
 */
public class CalculatorTest {
    @Test
    public void solve() throws Exception {
        Calculator calculator = new Calculator();
        assertEquals(6.0,calculator.solveEquation("2+4"),0.1);
        assertEquals(-2.0,calculator.solveEquation("2-4"),0.1);
        assertEquals(8.0,calculator.solveEquation("2*4"),0.1);
        assertEquals(0.5,calculator.solveEquation("2/4"),0.1);
        assertEquals(2.0,calculator.solveEquation("2"),0.1);
        assertEquals(5.5,calculator.solveEquation("2/4+5"),0.1);
        assertEquals(14.0,calculator.solveEquation("2+3*4"),0.1);
        assertEquals(8.0,calculator.solveEquation("2+3*4/2"),0.1);
    }

}