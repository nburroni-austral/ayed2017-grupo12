package main.tp8;

import main.tp11.ScannerUtil;
import org.junit.Test;

public class ScannerUtilTest {

    @Test
    public void adapt() {
        ScannerUtil s = new ScannerUtil();
        String string = "ASDF ASDF ASDF ASDF";
        System.out.println(string);
        string = s.adapt(string, 15);
        System.out.println(string);
    }
}