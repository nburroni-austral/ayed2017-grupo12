package main.tp1.SortAlgorithms;

import static main.tp1.SearchAlgorithms.RandomIntArray.randomIntArray;
import static org.junit.Assert.*;

/**
 * Created by arimi on 13-Mar-17.
 */
public class BubbleSortTest {
    @org.junit.Test
    public void bubbleSortTest() throws Exception {
        Integer[] ints = randomIntArray(10);
        for (int i : ints){
            System.out.print(i + ",  ");
        }

        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.bubbleSort(ints);

        System.out.println("\n");
        for (int i : ints){
            System.out.print(i + ",  ");
        }
    }

}