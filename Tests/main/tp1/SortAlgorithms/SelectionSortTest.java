package main.tp1.SortAlgorithms;

import org.junit.Test;

import static main.tp1.SearchAlgorithms.RandomIntArray.randomIntArray;
/**
 * Created by arimi on 12-Mar-17.
 */
public class SelectionSortTest {
    @Test
    public void selectionSortTest() throws Exception {
        Integer[] ints = randomIntArray(10);
        for (int i : ints){
            System.out.print(i + ",  ");
        }
        SelectionSort selectionSort = new SelectionSort();
        selectionSort.selectionSort(ints);

        System.out.println("\n");
        for (int i : ints){
            System.out.print(i + ",  ");
        }

        System.out.println();
        System.out.println();


        Integer[] ints2 = randomIntArray(10);
        for (int i : ints2){
            System.out.print(i + ",  ");
        }

        selectionSort.recursiveSelectionSort(ints2);

        System.out.println("\n");
        for (int i : ints2){
            System.out.print(i + ",  ");
        }
    }
}
