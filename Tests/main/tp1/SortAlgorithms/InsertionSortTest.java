package main.tp1.SortAlgorithms;

import org.junit.Test;

import static main.tp1.SearchAlgorithms.RandomIntArray.randomIntArray;
import static org.junit.Assert.*;

/**
 * Created by arimi on 12-Mar-17.
 */
public class InsertionSortTest {
    @Test
    public void insertionSortTest() throws Exception {
        Integer[] ints = randomIntArray(10);
        for (int i : ints){
            System.out.print(i + ",  ");
        }

        InsertionSort insertionSort = new InsertionSort();
        insertionSort.insertionSort(ints);

        System.out.println("\n");
        for (int i : ints){
            System.out.print(i + ",  ");
        }

    }

}