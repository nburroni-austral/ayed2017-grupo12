package main.tp1;

import static main.tp1.SearchAlgorithms.RandomIntArray.randomIntArray;
import static main.tp1.SearchAlgorithms.BinarySearch.contains;
import static org.junit.Assert.assertEquals;

/**
 * Created by arimi on 12-Mar-17.
 */
public class BinarySearchTest {
    @org.junit.Test
    public void containsTest() throws Exception {
        int[] ints = {1,2,3,4,5,6,7,8,9,10,11};
        assertEquals(1,contains(ints,2));
        assertEquals(-1,contains(ints,77));
    }
}