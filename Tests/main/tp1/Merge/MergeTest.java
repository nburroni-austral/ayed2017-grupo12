package main.tp1.Merge;

import main.tp1.SortAlgorithms.InsertionSort;
import org.junit.Test;

import static main.tp1.SearchAlgorithms.RandomIntArray.randomIntArray;
import static org.junit.Assert.*;

/**
 * Created by arimi on 14-Mar-17.
 */
public class MergeTest {
    @Test
    public void merge() throws Exception {
        Integer[] ints = randomIntArray(10);

        InsertionSort insertionSort = new InsertionSort();
        insertionSort.insertionSort(ints);

        Integer[] ints2 = randomIntArray(10);

        insertionSort.insertionSort(ints2);

        for (int i : ints2){
            System.out.print(i + ",  ");
        }

        System.out.println("\n");
        for (int i : ints){
            System.out.print(i + ",  ");
        }
        System.out.println("\n");

        Merge merge = new Merge();
        Comparable[] intsMerged = merge.merge(ints,ints2);

        for (Comparable i : intsMerged){
            System.out.print(i + ",  ");
        }
    }

}