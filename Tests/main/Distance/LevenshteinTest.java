package main.Distance;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by arimi on 15-Mar-17.
 */
public class LevenshteinTest {
    @Test
    public void levenshteinDistance() throws Exception {
        Levenshtein l = new Levenshtein();

        String casa = "casa";
        String calle = "calle";
        assertEquals(3,l.levenshteinDistance(casa,calle));

        String batata = "batata";
        assertEquals(4,l.levenshteinDistance(batata, casa));

        String love = "love";
        String hate = "hate";
        assertEquals(3,l.levenshteinDistance(love, hate));

        assertEquals(1,l.levenshteinDistance(love, "lover"));
    }

}