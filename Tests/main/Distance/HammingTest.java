package main.Distance;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by arimi on 15-Mar-17.
 */
public class HammingTest {
    @Test
    public void hammingDistance() throws Exception {
        Hamming h = new Hamming();
        String a = "string1";
        String b = "string2";
        assertEquals(h.hammingDistance(a,b),1);

    }

}