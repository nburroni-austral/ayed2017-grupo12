package main.tp9;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HashTableTest {

    @Test
    public void HashTableTests() {

        HashTable hashTable = new HashTable(100000);

        hashTable.put("beer");
        hashTable.put("bear");
        hashTable.put( "bearer");
        hashTable.put("car");
        hashTable.put("building");

        List<String> wordsList = hashTable.get("B600");

        assertTrue(wordsList.size() == 2);
        assertTrue(wordsList.get(0).equals("beer"));
        assertTrue(wordsList.get(1).equals("bear"));

        hashTable.removeSoundexCode("B600");

        hashTable.removeWord("car");

        assertFalse(hashTable.containsSoundexCode("B600"));
        assertTrue(hashTable.containsSoundexCode("B435"));

        assertFalse(hashTable.containsWord("beer"));
        assertTrue(hashTable.containsWord("bearer"));

        hashTable.printTable();
    }
}
