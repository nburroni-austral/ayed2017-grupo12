package main.tp9;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SoundexTest {

    @Test
    public void SoundexTests() {

        Soundex soundex = new Soundex();

        assertTrue(soundex.getCode("beer").equals("B600"));
        assertTrue(soundex.getCode("bear").equals("B600"));
        assertTrue(soundex.getCode("bearer").equals("B660"));
    }
}
