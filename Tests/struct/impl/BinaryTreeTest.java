package struct.impl;

import org.junit.Test;
import static org.junit.Assert.*;
import main.tp7.*;
import main.tp11.SerializationUtil;

import java.util.ArrayList;

public class BinaryTreeTest {
    @Test
    @SuppressWarnings("unchecked")
    public void test() {
        //api SerializationUtil and binary trees
        SerializationUtil s = new SerializationUtil();
        TreeApi api = new TreeApi();
        BinaryTree<Integer> tree1 = new BinaryTree<>();
        BinaryTree<Integer> tree11 = new BinaryTree<>(11);
        BinaryTree<Integer> tree2 = new BinaryTree<>(2);
        BinaryTree<Integer> tree3 = new BinaryTree<>(3, tree11, tree2);
        BinaryTree<Integer> treeMult = new BinaryTree<>(10,tree3,tree3);
        BinaryTree<String> treeA = new BinaryTree<>("A");
        BinaryTree<String> treeC = new BinaryTree<>("C");
        BinaryTree<String> treeE = new BinaryTree<>("E");
        BinaryTree<String> treeH = new BinaryTree<>("H");
        BinaryTree<String> treeDCE = new BinaryTree<>("D",treeC,treeE);
        BinaryTree<String> treeBAD = new BinaryTree<>("B",treeA,treeDCE);
        BinaryTree<String> treeIH = new BinaryTree<>("I",treeH,new BinaryTree<>());
        BinaryTree<String> treeGIH = new BinaryTree<>("G",new BinaryTree<>(),treeIH);
        BinaryTree<String> treeComplete = new BinaryTree<>("F",treeBAD,treeGIH);
        //isEmpty
        assertEquals(true,tree1.isEmpty());
        assertEquals(false,tree2.isEmpty());
        //weight
        assertEquals(1,api.weight(tree2));
        assertEquals(3,api.weight(tree3));
        //getRoot
        assertEquals(3,tree3.getRoot(),0.01);
        //leaves
        assertEquals(2,api.leaves(tree3),0.01);
        assertEquals(0,api.leaves(tree1),0.01);
        //occurrences
        assertEquals(1,api.occurrences(11,tree11),0.01);
        assertEquals(1,api.occurrences(3,tree3),0.01);
        assertEquals(2,api.occurrences(3,treeMult),0.01);
        //elementsInLevel
        assertEquals(0,api.elementsInLevel(tree1,0),0.01);
        assertEquals(2,api.elementsInLevel(tree3,1),0.01);
        assertEquals(4,api.elementsInLevel(treeMult,2),0.01);
        //height
        assertEquals(1,api.height(tree3),0.01);
        assertEquals(2,api.height(treeMult),0.01);
        //sum
        assertEquals(16,api.integerTreeSum(tree3),0.01);
        assertEquals(42,api.integerTreeSum(treeMult),0.01);
        assertEquals(2,api.integerTreeSum(tree2),0.01);
        assertEquals(0,api.integerTreeSum(tree1),0.01);
        assertEquals(11,api.integerTreeSum(tree11),0.01);
        //threeMultipleSum
        assertEquals(3,api.threeMultipleSum(tree3));
        assertEquals(6,api.threeMultipleSum(treeMult));
        assertEquals(0,api.threeMultipleSum(tree2));
        //equals similar isomorphic
        assertEquals(false,api.equals(tree1,tree2));
        assertEquals(true,api.equals(tree2,tree2));
        assertEquals(true,api.equals(treeMult,treeMult));
        //treeOccurrence
        assertEquals(true,api.treeOccurrence(treeMult,tree3));
        assertEquals(false,api.treeOccurrence(tree3,tree1));
        //isFull
        assertEquals(true,api.isFull(treeMult));
        assertEquals(false,api.isFull(tree1));
        //isStable
        assertEquals(true,api.isStable(tree1));
        assertEquals(true,api.isStable(tree2));
        //frontier
        System.out.println("frontier: ");
        api.showFrontier(tree3);
        System.out.println();
        ArrayList<Integer> list = api.frontier(tree3);
        assertEquals(2,list.size());
        //serialization
        s.writeObject(tree3,"tree3");
        BinaryTree<Integer> deserializedTree = (BinaryTree<Integer>) s.readObject("tree3");
        assertTrue(tree3.getRoot().equals(deserializedTree.getRoot()));
        assertTrue(api.equals(tree3,deserializedTree));
        assertTrue(!deserializedTree.isEmpty());
        //pre-order
        System.out.println("pre-order: ");
        api.preOrder(treeComplete);
        System.out.println("\n");
        //in-order
        System.out.println("in-order: ");
        api.inOrder(treeComplete);
        System.out.println("\n");
        //post-order
        System.out.println("post-order: ");
        api.postOrder(treeComplete);
        System.out.println("\n");
        //byLevels
        System.out.println("by levels: ");
        api.byLevels(treeComplete);
        System.out.println("\n");
        System.out.println();
    }

}