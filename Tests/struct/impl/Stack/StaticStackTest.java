package struct.impl.Stack;

import struct.impl.StaticStack;
import org.junit.Test;

import static org.junit.Assert.*;

public class StaticStackTest {
    @Test
    public void push() throws Exception {
        StaticStack<Integer> stack = new StaticStack<>(10);
        stack.push(new Integer(1));
        assertEquals(1,stack.size());
    }

    @Test
    public void pop() throws Exception {
        StaticStack<Integer> stack = new StaticStack<>(10);
        stack.push(new Integer(1));
    }

    @Test
    public void peek() throws Exception {
        StaticStack<Integer> stack = new StaticStack<>(10);
        stack.push(new Integer(1));
        Integer j = stack.peek();
        assertEquals(java.util.Optional.ofNullable(1), java.util.Optional.ofNullable(j));
    }

    @Test
    public void isEmpty() throws Exception {
        StaticStack<Integer> stack = new StaticStack<>(10);
        stack.push(new Integer(1));
        assertEquals(false,stack.isEmpty());
    }

    @Test
    public void size() throws Exception {
        StaticStack<Integer> stack = new StaticStack<>(10);
        stack.push(new Integer(1));
        assertEquals(1,stack.size());
    }

    @Test
    public void empty() throws Exception {
        StaticStack<Integer> stack = new StaticStack<>(10);
        stack.push(new Integer(1));
        stack.empty();
        assertEquals(0,stack.size());
    }

}