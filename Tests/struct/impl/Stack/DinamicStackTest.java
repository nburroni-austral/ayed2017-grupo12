package struct.impl.Stack;

import org.junit.Test;
import struct.impl.DynamicStack;

import static org.junit.Assert.*;

public class DinamicStackTest {
    @Test
    public void push() throws Exception {
        DynamicStack<Integer> stack = new DynamicStack<>();
        stack.push(1);
        assertEquals(1,stack.size());

    }

    @Test
    public void pop() throws Exception {
        DynamicStack<Integer> stack = new DynamicStack<>();
        stack.push(1);
        stack.pop();
        assertEquals(0,stack.size());
    }

    @Test
    public void peek() throws Exception {
        DynamicStack<Integer> stack = new DynamicStack<>();
        stack.push(1);
        assertEquals(java.util.Optional.of(1),java.util.Optional.of(stack.peek()));
    }

    @Test
    public void isEmpty() throws Exception {
        DynamicStack<Integer> stack = new DynamicStack<>();
        stack.push(1);
        assertEquals(false, stack.isEmpty());
        stack.pop();
        assertEquals(true, stack.isEmpty());
    }

    @Test
    public void size() throws Exception {
        DynamicStack<Integer> stack = new DynamicStack<>();
        stack.push(1);
        assertEquals(1,stack.size());
    }

    @Test
    public void empty() throws Exception {
        DynamicStack<Integer> stack = new DynamicStack<>();
        stack.push(1);
        stack.empty();
        assertEquals(0,stack.size());
        assertEquals(true, stack.isEmpty());

    }

}