package struct.impl.Queue;

import org.junit.Test;
import struct.impl.DynamicQueue;

import static org.junit.Assert.*;

public class DynamicQueueTest {
    @Test
    public void test1() throws Exception {
        DynamicQueue<Integer> queue = new DynamicQueue();
        queue.enqueue(1);
        assertEquals(1,queue.size());
        queue.enqueue(2);
        assertEquals(new Integer(1),queue.dequeue());
        assertEquals(new Integer(2),queue.dequeue());
        queue.enqueue(3);
        queue.enqueue(4);
        queue.empty();
        assertEquals(null,queue.dequeue());
        queue.enqueue(5);
        queue.enqueue(1);
        queue.enqueue(3);
        assertEquals(5,queue.dequeue(),0.1);
        assertEquals(1,queue.dequeue(),0.1);
        assertEquals(3,queue.dequeue(),0.1);
        assertEquals(true,queue.isEmpty());
        queue.enqueue(7);
        assertEquals(false,queue.isEmpty());
        queue.empty();
        assertEquals(true,queue.isEmpty());

    }

}