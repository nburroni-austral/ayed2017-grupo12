package struct.impl.Queue;

import struct.impl.StaticQueue;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by arimi on 05-Apr-17.
 */
public class StaticQueueTest {
    @Test
    public void enqueue() throws Exception {
        StaticQueue<Integer> queue = new StaticQueue<>(2);
        queue.enqueue(1);
        assertEquals(1,queue.size());
        queue.enqueue(2);
        assertEquals(new Integer(1),queue.dequeue());
        assertEquals(new Integer(2),queue.dequeue());
        queue.enqueue(3);
        queue.enqueue(4);
        queue.empty();
        assertEquals(null,queue.dequeue());
        queue.enqueue(5);
        queue.enqueue(1);
        queue.enqueue(3);
        assertEquals(5,queue.dequeue(),0.1);
        assertEquals(1,queue.dequeue(),0.1);
        assertEquals(3,queue.dequeue(),0.1);
        assertEquals(true,queue.isEmpty());
        queue.enqueue(7);
        assertEquals(false,queue.isEmpty());
        queue.empty();
        assertEquals(true,queue.isEmpty());

    }

}