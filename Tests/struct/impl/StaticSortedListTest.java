package struct.impl;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class StaticSortedListTest {
    @Test
    public void insert() throws Exception {
        StaticSortedList<Integer> ssl = new StaticSortedList<>();
        DynamicSortedList<Integer> dsl = new DynamicSortedList<>();

        /*
        for (int i = 0; i < 10; i++){
            Random r = new Random();
            ssl.insert(r.nextInt(11));
            dsl.insert(r.nextInt(11));
        }
        */

        ssl.insert(5);
        ssl.insert(2);
        ssl.insert(0);
        ssl.insert(10);
        ssl.insert(11);
        dsl.insert(5);
        dsl.insert(2);
        dsl.insert(0);
        dsl.insert(10);
        dsl.insert(11);

        System.out.println("dynamic sorted List: ");

        for (int i = 0; i < dsl.size(); i++){
            dsl.goTo(i);
            System.out.print(dsl.getActual() + "  ");
        }
        System.out.println("");

        System.out.println("static sorted List: ");

        for (int j = 0; j < ssl.size(); j++){
            ssl.goTo(j);
            System.out.print(ssl.getActual() + "  ");
        }
    }

}