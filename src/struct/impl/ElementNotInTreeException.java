package struct.impl;
public class ElementNotInTreeException extends RuntimeException{
    public ElementNotInTreeException(){
        super("the element searched is not contained in the tree");
    }
}
