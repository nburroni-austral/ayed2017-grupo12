package struct.impl;
import struct.istruct.Queue;

public class DynamicQueue<Q> implements Queue<Q>{
    private Node front;
    private Node back;
    private int size = 0;

    @Override
    @SuppressWarnings("unchecked")
    public void enqueue(Q q) {
        Node aux = new Node(q);
        aux.next = null;
        if (front == null) {
            front = aux;
            back = aux;
        }
        else{
            back.next = aux;
            back = aux;
        }
        size++;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Q dequeue() {
        if (size > 1){
            Q result = (Q) front.data;
            front = front.next;
            size--;
            return result;
        }
        else if (size == 1){
            Q result = (Q) front.data;
            empty();
            return result;
        }
        else return null;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int length() {
        return size;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        front = null;
        back = null;
        size = 0;
    }
}
