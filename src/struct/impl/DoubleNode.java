package struct.impl;

import java.io.Serializable;

/***
 * DoubleNode class creates Double Nodes for usage in trees data structures
 * @param <T>
 */
public class DoubleNode<T> implements Serializable{
    T data;
    DoubleNode<T> right, left;

    //serialization Key
    private static final long serialVersionUID = -55857686305273843L;
    /***
     * creates a double node with an element and no left or right pointers
     * @param data the element of the double node
     */
    public DoubleNode(T data) {
        this.data = data;
    }

    /***
     * creates a double node with an element and a left and right pointers
     * @param data the element of the double node
     */
    public DoubleNode (T data, DoubleNode<T> left, DoubleNode<T> right){
        this.data = data;
        this.left = left;
        this.right = right;
    }
}
