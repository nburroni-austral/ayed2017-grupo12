package struct.impl;
import struct.istruct.Queue;
public class StaticQueue<Q> implements Queue<Q> {
    private int front,back,size;
    private Object[] data;

    public StaticQueue(int initialCapacity) {
        data = new Object[initialCapacity];
    }

    @Override
    public void enqueue(Q q) {
        if (size == length()) grow();
        data[back] = q;
        back = increment(back);
        size++;
    }

    @Override
    public Q dequeue() {
        if (isEmpty()) return null;
        Q result = (Q) data[front];
        front = increment(front);
        size--;
        return result;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int length() {
        return data.length;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        back = 0;
        front = 0;
        size = 0;
    }

    private int increment(int a){
        if(a < length()-1) return a+1;
        return 0;
    }

    private void grow(){
        Object[] aux = new Object[length()*2];
        for (int i = 0; i < length(); i++){
            aux[i] = dequeue();
        }
        front = 0;
        back = data.length;
        size = data.length;
        data = aux;
    }
}