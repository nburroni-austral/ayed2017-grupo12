package struct.impl;

import java.io.Serializable;

/***
 * BinaryTree class creates binary trees of a specified type, with it's method to operate
 * @param <T> the type of the binary tree
 */
public class BinaryTree<T> implements struct.istruct.BinaryTree<T>,Serializable{
    private DoubleNode<T> root;

    //serialization Key
    private static final long serialVersionUID = -55857686305273843L;
    /***
     * creates an empty tree
     */
    public BinaryTree() {root = null;}

    /***
     * creates a tree with a root element
     * @param data the element
     */
    public BinaryTree(T data) {root = new DoubleNode<>(data);}

    /***
     * creates a tree with a root element and two descendants
     * @param data the root element
     * @param leftTree the left descendant
     * @param rightTree the right descendant
     */
    public BinaryTree(T data, BinaryTree<T> leftTree, BinaryTree<T> rightTree){
        root = new DoubleNode<T>(data,leftTree.root, rightTree.root);
    }

    /***
     * checks if the binary tree is empty
     * @return true if the binary tree is empty, false if it's not
     */
    public boolean isEmpty(){
        return root == null;
    }

    /***
     * returns the element in the root of the binary tree
     * @return the element in the root
     */
    public T getRoot(){
        return root.data;
    }

    /***
     * returns the left tree descendant
     * @return the left tree descendant
     */
    public BinaryTree<T> getLeft(){
        BinaryTree<T> aux = new BinaryTree<>();
        aux.root = root.left;
        return aux;
    }

    /***
     * returns the right tree descendant
     * @return the right tree descendant
     */
    public BinaryTree<T> getRight(){
        BinaryTree<T> aux = new BinaryTree<T>();
        aux.root = root.right;
        return aux;
    }
}
