package struct.impl;

import struct.istruct.Stack;

/**
 * Created by arimi on 22-Mar-17.
 */
public class StaticStack<T> implements Stack<T> {
    private int top;
    private int capacity;
    private Object[] data;

    public StaticStack(int capacity) {
        this.capacity = capacity;
        top = -1;
        data = new Object[capacity];
    }

    public void push(T t) {
        if (top + 1 == data.length) grow();
        top++;
        data[top] = t;
    }

    private void grow() {
        Object[] data2 = new Object[capacity*2];
        for (int i = 0; i < capacity; i++){
            data2[i] = data[i];
        }
        data = data2;
    }

    public void pop() {
        top--;
    }

    public T peek() {
        if (!isEmpty()) return (T) data[top];
        return null;
    }

    public boolean isEmpty() {
        if (top == -1) return true;
        return false;
    }

    public int size() {
        return top + 1;
    }

    public void empty() {
        top = -1;
    }
}
