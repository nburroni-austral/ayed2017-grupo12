package struct.impl;

import struct.istruct.Stack;

/**
 * Created by arimi on 22-Mar-17.
 */
public class DynamicStack<T> implements Stack<T> {
    private Node top;
    private int size = 0;
    @SuppressWarnings("unchecked")
    public void push(T t) {
        Node newTop = new Node(t);
        newTop.next = top;
        top = newTop;
        size++;
    }

    public void pop() {
        top = top.next;
        size--;
    }
    @SuppressWarnings("unchecked")
    public T peek() {
        return (T) top.data;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void empty() {
        top = null;
        size = 0;
    }
}
