package struct.impl;

/**
 * implementation of a not balanced search binary tree
 * @param <T> type of the tree
 */
public class SearchBinaryTree<T extends Comparable<T>> implements struct.istruct.BinaryTree<T> {

    private DoubleNode<T> root;

    public SearchBinaryTree(){
        root = null;
    }

    /**
     * checks if the tree is empty
     * @return true if it is, false if it's not
     */
    @Override
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * returns the element in the root
     * @return the element in the root
     */
    @Override
    public T getRoot() {
        return root.data;
    }

    /**
     * returns the tree on the left
     * @return the tree on the left
     */
    @Override
    public SearchBinaryTree<T> getLeft() {
        SearchBinaryTree<T> aux = new SearchBinaryTree<>();
        aux.root = root.left;
        return aux;

    }

    /**
     * returns the tree on the right
     * @return the tree on the right
     */
    @Override
    public SearchBinaryTree<T> getRight() {
        SearchBinaryTree<T> aux = new SearchBinaryTree<>();
        aux.root = root.right;
        return aux;
    }

    /**
     * checks if an element is contained in the tree
     * @param t the element
     * @return true if it is, false if it's not
     */
    public boolean exists(T t){
        return true;
    }

    /**
     * @return the Minimum element of the tree
     */
    public T getMin(){
        return getMin(root).data;
    }

    /**
     * @return the Maximum element of the tree
     */
    public T getMax(){
        return getMax(root).data;
    }

    /**
     * searches the tree for a specific element
     * @param t the element searched
     * @return the element searched for
     */
    public T search(T t){
        return search(root,t).data;
    }

    /**
     * inserts an element in the tree
     * @param t the element to be inserted
     */
    public void insert(T t){
        root = insert(root,t);
    }

    /**
     * removes an element from the tree
     * @param t the element to be removed
     */
    public void remove(T t){
        root = remove(root,t);
    }

    //private methods
    private boolean exists(DoubleNode<T> node, T t){
        if (t == null) return false;
        if (t.compareTo(node.data) == 0) return true;
        else if (t.compareTo(node.data) < 0) return exists(node.left,t);
        else return exists(node.right,t);
    }

    private DoubleNode<T> getMin(DoubleNode<T> t){
        if (t.left == null) return t;
        else return getMin(t.left);
    }

    private DoubleNode<T> getMax(DoubleNode<T> t){
        if (t.right == null) return t;
        else return getMin(t.right);
    }

    private DoubleNode<T> search(DoubleNode<T> node, T t){
        if (t == null) throw new ElementNotInTreeException();
        if (t.compareTo(node.data) == 0) return node;
        else if (t.compareTo(node.data) < 0) return search(node.left,t);
        else return search(node.right,t);
    }

    private DoubleNode<T> insert(DoubleNode<T> node, T t){
        if (node == null) node = new DoubleNode<T>(t);
        else if (t.compareTo(node.data) < 0) node.left = insert(node.left,t);
        else node.right = insert(node.right,t);
        return node;
    }

    private DoubleNode<T> remove(DoubleNode<T> node, T t){
        if (t.compareTo(node.data) < 0) node.left = remove(node.left, t);
        else if (t.compareTo(node.data) > 0) node.right = remove(node.right, t);
        else { //remove item found
            if (node.left != null && node.right != null) {
                node.data = getMin(node.right).data;
                node.right = removeMin(node.right);
            }
            else if (node.left != null) node = node.left;
            else node = node.right;
        }
        return node;
    }

    private DoubleNode<T> removeMin(DoubleNode<T> node) {
        if (node.left != null) node.left = removeMin(node.left);
        else node = node.right;
        return node;
    }
}

