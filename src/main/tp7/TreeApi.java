package main.tp7;
import struct.istruct.BinaryTree;

import java.util.ArrayList;

/***
 * TreeApi class has methods to operate with BinaryTrees
 * @param <T>
 */
public class TreeApi<T> {

    /**
     * Calculates the weight of a binary tree (number of descendants + 1)
     * @param tree binary tree
     * @return weight of the tree
     */
    public int weight(BinaryTree<T> tree) {
        if (tree.isEmpty()) return 0;
        return weight(tree.getLeft()) + weight(tree.getRight())+ 1;
    }

    /**
     * Calculates the amount of leaves of a binary tree.
     * @param tree binary tree
     * @return the amount of leaves the tree has
     */
    public int leaves(BinaryTree<T> tree){
        if (tree.isEmpty()) return 0;
        if(tree.getLeft().isEmpty() && tree.getRight().isEmpty()) return 1;
        return leaves(tree.getLeft()) + leaves(tree.getRight());
    }

    /***
     * calculates the times an element is present on a tree
     * @param data element to be searched
     * @param tree tree to be searched on
     * @return number of occurrence of the element
     */
    public int occurrences(T data, BinaryTree<T> tree){
        if (tree.isEmpty()) return 0;
        if (tree.getRoot().equals(data)) return 1;
        return occurrences(data, tree.getLeft()) + occurrences(data, tree.getLeft());
    }

    /***
     * Calculates the amount of elements in a specific level of a binary tree
     * @param tree binaryTree
     * @param level level to be analysed
     * @return amount of elements in that level
     */
    public int elementsInLevel(BinaryTree<T> tree,int level){
        if (tree.isEmpty()) return 0;
        return elementsInLevelAux(tree,level,0);
    }


    public int elementsInLevelAux(BinaryTree<T> tree,int level, int aux){
        if (tree.isEmpty()) return 0;
        if (aux == level){
            if (!tree.isEmpty()) return 1;
            return 0;
        }
        return (elementsInLevelAux(tree.getLeft(),level,aux+1) + elementsInLevelAux(tree.getRight(),level,aux+1));
    }


    /**
     * Calculates the height of a binary tree (length of the path between the root and its most distant leaf)
     * @param tree binary tree
     * @return the height of the binary tree
     */
    public int height(BinaryTree<T> tree){
        return heightAux(tree,0);
    }

    private int heightAux(BinaryTree<T> tree, int aux){
        if (tree.getRight().isEmpty() && tree.getLeft().isEmpty()) return aux;
        return Math.max(heightAux(tree.getLeft(),aux+1), heightAux(tree.getRight(),aux+1));
    }

    /**
     * Calculates the sum of all the elements in an Integer binary tree
     * @param tree binary tree of integers
     * @return sum of all elements in the tree
     */
    public Integer integerTreeSum(BinaryTree<Integer> tree){
        if (tree.isEmpty()) return 0;
        return integerTreeSumAux(tree,0);
    }

    private int integerTreeSumAux(BinaryTree<Integer> tree, Integer aux){
        if (tree.getRight().isEmpty() && tree.getLeft().isEmpty()) return tree.getRoot();
        return tree.getRoot()
                + integerTreeSumAux(tree.getLeft(),aux + tree.getLeft().getRoot())
                + integerTreeSumAux(tree.getRight(),aux + tree.getRight().getRoot());
    }

    /***
     * Calculates the sum of all the elements that are multiple of 3 in an Integer binary tree
     * @param tree binary tree of integers
     * @return sum of the elements in the tree
     */
    public int threeMultipleSum (BinaryTree<Integer> tree){
        if (tree.isEmpty()) return 0;
        else if ((tree.getRoot() % 3) == 0) return tree.getRoot() + threeMultipleSum(tree.getLeft()) + threeMultipleSum(tree.getRight());
        else return threeMultipleSum(tree.getLeft()) + threeMultipleSum(tree.getRight());
    }

    /***
     * checks if two trees are isomorphic, if they have the same shape
     * @param t1 one Binary Tree
     * @param t2 the other Binary Tree
     * @return true if they are isomorphic, false if they aren't
     */
    public boolean isomorphic(BinaryTree t1, BinaryTree t2){
        if(t1.isEmpty() && t2.isEmpty()) return true;
        if(t1.isEmpty() || t2.isEmpty()) return false;
        return isomorphic(t1.getLeft(), t2.getLeft()) && isomorphic(t1.getRight(), t2.getRight());
    }

    /**
     * checks if two trees are similar, if they have the same elements
     * @param t1 one Binary Tree
     * @param t2 the other Binary Tree
     * @return true if they are similar, false if they aren't
     */
    public boolean similar(BinaryTree t1, BinaryTree t2){
        ArrayList elements1 = getElements(t1,new ArrayList());
        ArrayList elements2 = getElements(t2,new ArrayList());
        if(elements1.size() == elements2.size()){
            elements1.removeAll(elements2);
            return elements1.isEmpty();
        }
        return false;
    }

    private ArrayList getElements(BinaryTree b, ArrayList list){
        if(b.isEmpty()) return list;
        list.add(b.getRoot());
        getElements(b.getLeft(), list);
        getElements(b.getRight(), list);
        return list;
    }

    /**
     * checks if two trees are equal, if they are the same tree
     * @param t1 one Binary Tree
     * @param t2 the other Binary Tree
     * @return true if they are equal, false if they arent
     */
    public boolean equals(BinaryTree t1, BinaryTree t2){
        if (t1.isEmpty() && !t2.isEmpty() || !t1.isEmpty() && t2.isEmpty() ) return false;
        if (t1.isEmpty() && t2.isEmpty()) return true;
        return t1.getRoot().equals(t2.getRoot())
                && equals(t1.getLeft(),t2.getLeft())
                && equals(t1.getRight(),t2.getRight());

    }


    public boolean isComplete(BinaryTree t){
        if (t.isEmpty()) return false; //(?)
        if(t.getLeft().isEmpty() && !t.getRight().isEmpty() || !t.getLeft().isEmpty() && t.getRight().isEmpty())
            return false;
        if(t.getLeft().isEmpty() && t.getRight().isEmpty())
            return true;
        return isComplete(t.getLeft()) && isComplete(t.getRight());
    }

    /**
     * checks if a tree is full, if all it's descendants have also two descendants
     * @param t one Binary Tree
     * @return true if it's full, false if it's not
     */
    public boolean isFull(BinaryTree<T> t){
        if (t.isEmpty()) return false;
        int height = height(t);
        return isComplete(t) && Math.pow(2,height) == elementsInLevel(t,height);
    }

    public <T extends Comparable<T>> boolean isStable(BinaryTree<T> t){
        boolean result = true;// added because there are multiple if's and a main boolean must be saved
        if(t.isEmpty() || (t.getLeft().isEmpty() && t.getRight().isEmpty()))
            return true;
        if(!t.getRight().isEmpty()){
            result = t.getRoot().compareTo(t.getRight().getRoot()) >= 0;
        }
        if(!t.getLeft().isEmpty()){
            result &= t.getRoot().compareTo(t.getLeft().getRoot()) >= 0;
        }
        return result && isStable(t.getLeft()) && isStable(t.getRight());
    }

    /***
     * checks if a binary tree is contained in other tree
     * @param tree one Binary Tree
     * @param searched the Binary Tree that's being searched for
     * @return true if the tree contains the searched one, false if not
     */
    public boolean treeOccurrence(BinaryTree<T> tree,BinaryTree<T> searched){
        if (searched.isEmpty() || tree.isEmpty()) return false;
        else
            return equals(tree, searched)
                    || treeOccurrence(tree.getLeft(), searched)
                    || treeOccurrence(tree.getLeft(), searched);
    }

    /***
     * prints the elements of the leaves of a tree
     * @param t the tree
     */
    public void showFrontier(BinaryTree t){
        if(t.getLeft().isEmpty() && t.getRight().isEmpty()) {
            System.out.print(t.getRoot().toString() + "   ");
            return;
        }
        if(!t.getLeft().isEmpty())
            showFrontier(t.getLeft());
        if(!t.getRight().isEmpty())
            showFrontier(t.getRight());
    }

    /***
     * saves the frontier of a binary tree in a list
     * @param t the binary tree
     * @param <T> the type of the elements
     * @return the list
     */
    public<T> ArrayList<T> frontier(BinaryTree<T> t){
        ArrayList<T> list = new ArrayList<T>();
        binaryTreeFrontierAux(t,list);
        return list;
    }

    private<T> void binaryTreeFrontierAux(BinaryTree<T> t, ArrayList<T> list){
        if(t.getLeft().isEmpty() && t.getRight().isEmpty()) {
            list.add(t.getRoot());
            return;
        }
        if(!t.getLeft().isEmpty())
            binaryTreeFrontierAux(t.getLeft(), list);
        if(!t.getRight().isEmpty())
            binaryTreeFrontierAux(t.getRight(), list);
    }

    /**
     * prints the tree in pre-order
     * @param t the tree to be shown
     */
    public void preOrder(BinaryTree<T> t){
        if (t.isEmpty()) return;
        System.out.print(t.getRoot() + "  ");
        preOrder(t.getLeft());
        preOrder(t.getRight());
    }

    /**
     * prints the tree in in-order
     * @param t the tree to be shown
     */
    public void inOrder(BinaryTree<T> t){
        if (t.isEmpty()) return;
        inOrder(t.getLeft());
        System.out.print(t.getRoot() + "  ");
        inOrder(t.getRight());
    }

    /**
     * prints the tree in post-order
     * @param t the tree to be shown
     */
    public void postOrder(BinaryTree<T> t) {
        if (t.isEmpty()) return;
        postOrder(t.getLeft());
        postOrder(t.getRight());
        System.out.print(t.getRoot() + "  ");
    }

    /***
     * prints a binary tree by levels
     * @param t the binary tree
     */
    public void byLevels(BinaryTree<T> t){
        System.out.println(byLevelString(t));
    }


    private String byLevelString(BinaryTree<T> t){
        String result = "";
        if(t.isEmpty()) return result;
        if(t.getLeft().isEmpty() || t.getRight().isEmpty()) result += t.getRoot() + "  ";
        ArrayList<ArrayList<Object>> elementList = byLevelAux(t,new ArrayList<>(), 0);
        for(ArrayList<Object> list: elementList){
            for(Object element: list){
                result += element + "  ";
            }
        }
        return result;
    }

    private ArrayList<ArrayList<Object>> byLevelAux(BinaryTree b, ArrayList<ArrayList<Object>> elementList, int level){
        if(!b.isEmpty()) {
            if(elementList.size() < level + 1) {
                elementList.add(new ArrayList<>());
            }
            elementList.get(level).add(b.getRoot());
            byLevelAux(b.getLeft(), elementList, level + 1);
            byLevelAux(b.getRight(), elementList, level + 1);
        }
        return elementList;
    }
}