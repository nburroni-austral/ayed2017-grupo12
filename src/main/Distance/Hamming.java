package main.Distance;

/**
 * Hamming class contains the hammingDistance method
 */
public class Hamming {

    /**
     * calculates the hamming distance between two given words
     * @param a
     * @param b
     * @return the hamming distance between words a and b
     */
    public int hammingDistance(String a, String b){
        int result = 0;
        if (a.length() != b.length()) throw new RuntimeException("not the same word length");
        for (int i = 0; i<a.length(); i++){
            if (a.charAt(i)!= b.charAt(i)){
                result++;
            }
        }
        return result;
    }
}