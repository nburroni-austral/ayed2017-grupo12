package main.Distance;

/**
 * Levenshtein class contains the method levenshteinDistance
 */
public class Levenshtein {
    /**
     * calculates the levenshtein distance between two words
     * @param a
     * @param b
     * @return the levenshtein distance between a and b
     */
    public int levenshteinDistance(String a, String b){

        int n = a.length();
        int m = b.length();
        int d[][] = new int[n+1][m+1];
        char a_i;
        char b_j;
        int cost;
        int result;

        if (m == 0) return n;
        if (n == 0) return m;

        for (int i = 0; i <= n; i++){
            d[i][0] = i;
        }

        for (int j = 0; j <= m; j++){
            d[0][j] = j;
        }

        for (int i = 1; i <= n; i++){
            a_i = a.charAt(i-1);

            for (int j = 1; j <= m; j++){
                b_j = b.charAt(j-1);
                if (a_i == b_j) cost = 0;
                else cost = 1;

                int above = d[i-1][j] + 1;
                int diagonalLeft = d[i-1][j-1] + cost;
                int left = d[i][j-1] + 1;
                d [i][j]= minimum(above,diagonalLeft,left);
            }
        }

        result = d[n][m];
        return result;
    }


    private int minimum(int a, int b, int c) {
        int min;

        min = a;
        if (b < min) {
            min = b;
        }
        if (c < min) {
            min = c;
        }
        return min;
    }
}