package main.KnightTour.GUI;

import main.KnightTour.GUI.View.ErrorWindow;
import main.KnightTour.GUI.View.KnightTourBoard;
import main.KnightTour.GUI.View.MainWindow;
import main.KnightTour.GUI.View.StartingPositionChoose;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

/**
 * The Controller class manages the functionality of MainWindow, StartingPositionChoose, and the KnightTourBoard.
 * It also adds Listeners to the buttons and their behaviour when clicked
 */
public class Controller {

    private MainWindow mainWindow;
    private StartingPositionChoose startingPositionChoose;
    private KnightTourBoard knightTourBoard;
    private ErrorWindow errorWindow;
    private String row, column;
    private int theRow, theColumn;

    public Controller() {

        this.mainWindow = new MainWindow();
        this.startingPositionChoose = new StartingPositionChoose();
        this.knightTourBoard = new KnightTourBoard();

        this.mainWindow.addStartListener(new StartTourListener());
        this.mainWindow.addExitListener(new ExitApplicationListener());
        this.startingPositionChoose.addConfirmListener(new ConfirmPositionListener());
        this.startingPositionChoose.addBackListener(new GoBackToMainWindowListener());
        this.knightTourBoard.addNextListener(new NextMovementListener());
        this.knightTourBoard.addExitListener(new ExitToStartingPositionChoose());
    }

    /**
     * Class to add listeners to the Start Button in the MainWindow, and implements it's behaviour when clicked
     */
    private class StartTourListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            mainWindow.hideSelf();
            startingPositionChoose.showSelf();
        }
    }

    /**
     * Class to add listeners to the Exit Button in the MainWindow, and implements it's behaviour when clicked
     */
    private class ExitApplicationListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }

    /**
     * Class to add listeners to the Confirm Button in the StartingPositionChoose window, and implements it's behaviour when clicked
     */
    public class ConfirmPositionListener implements ActionListener {

        int justOneTime = 0;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (justOneTime == 0) {
                row = startingPositionChoose.getRowEntered();
                column = startingPositionChoose.getColumnEntered();
                if (Pattern.matches("[a-zA-Z]+", row) || Pattern.matches("[a-zA-Z]+", column)) {
                    errorWindow = new ErrorWindow("Insert only numbers", startingPositionChoose);
                } else if (row.isEmpty() || column.isEmpty()) {
                    errorWindow = new ErrorWindow("Insert values in both fields", startingPositionChoose);
                } else {
                    theRow = Integer.parseInt(row);
                    theColumn = Integer.parseInt(column);
                    if (theRow < 1 || theRow > 8 || theColumn < 1 || theColumn > 8) {
                        errorWindow = new ErrorWindow("Insert numbers between 1 and 8", startingPositionChoose);
                    } else {
                        knightTourBoard.getKnightTour().setKnight(theRow, theColumn);
                        knightTourBoard.getKnightImages()[theRow-1][theColumn-1].setVisible(true);
                        startingPositionChoose.hideSelf();
                        knightTourBoard.showSelf();
                        justOneTime++;
                    }
                }
            } else {
                errorWindow = new ErrorWindow("Only one Knight can exist at a time. Restart the application to begin a new Tour", startingPositionChoose);
            }
        }
    }

    /**
     * Class to add listeners to the Back Button in the StartingPositionChoose window, and implements it's behaviour when clicked
     */
    public class GoBackToMainWindowListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            startingPositionChoose.hideSelf();
            mainWindow.showSelf();
        }
    }

    /**
     * Class to add listeners to the Next Button in the KnightTourBoard window, and implements it's behaviour when clicked
     */
    public class NextMovementListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            knightTourBoard.getKnightTour().move();
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (knightTourBoard.getKnightTour().getGrid()[i][j] == 1) {
                        knightTourBoard.getKnightImages()[i][j].setVisible(true);
                    } else if (knightTourBoard.getKnightTour().getGrid()[i][j] == -1) {
                        knightTourBoard.getKnightImages()[i][j].setVisible(false);
                    }
                }
            }
        }
    }

    /**
     * Class to add listeners to the Exit Button in the KnightTourBoard window, and implements it's behaviour when clicked
     */
    public class ExitToStartingPositionChoose implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            knightTourBoard.hideSelf();
            startingPositionChoose.showSelf();
        }
    }
}
