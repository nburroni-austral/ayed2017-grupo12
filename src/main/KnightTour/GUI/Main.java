package main.KnightTour.GUI;

/**
 * This Main class runs the Controller and initializes the program
 */
public class Main {

    public static void main(String[] args) {

        Controller controller = new Controller();
    }
}
