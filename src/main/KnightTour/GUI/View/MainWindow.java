package main.KnightTour.GUI.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * The Main Booth with all of its components
 */
public class MainWindow extends JFrame {

    private JPanel mainPanel;
    private JLabel title, thePicture;
    private JButton start, exit;

    public MainWindow() {

        this.setTitle("Main BusSystem");
        this.setSize(500, 500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        title = new JLabel("The Knight Tour");
        title.setFont(new Font("font", Font.BOLD, 50));
        title.setAlignmentX(CENTER_ALIGNMENT);

        thePicture = new JLabel();
        thePicture.setIcon(new ImageIcon(MainWindow.class.getResource("/main/KnightTour/GUI/View/Images/KnightImage.png")));
        thePicture.setAlignmentX(Box.CENTER_ALIGNMENT);

        start = new JButton("Start Knight Tour");
        start.setToolTipText("Start the Knight Tour");
        start.setSize(400, 300);
        start.setAlignmentX(CENTER_ALIGNMENT);

        exit = new JButton("Exit");
        exit.setToolTipText("Exit the application");
        exit.setSize(200, 100);
        exit.setAlignmentX(CENTER_ALIGNMENT);

        mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        mainPanel.setVisible(true);
        mainPanel.add(Box.createVerticalStrut(25));
        mainPanel.add(title);
        mainPanel.add(Box.createVerticalStrut(30));
        mainPanel.add(thePicture);
        mainPanel.add(Box.createVerticalStrut(60));
        mainPanel.add(start);
        mainPanel.add(Box.createVerticalStrut(25));
        mainPanel.add(exit);
        mainPanel.add(Box.createVerticalGlue());

        this.add(mainPanel);
        this.setVisible(true);
    }

    /**
     * Turns the window Visible
     */
    public void showSelf() {
        setVisible(true);
    }

    /**
     * Turns the window Invisible
     */
    public void hideSelf() {
        setVisible(false);
    }

    /**
     * Adds a Listener to the Start Button
     * @param listener is the action to perform
     */
    public void addStartListener(ActionListener listener) {
        start.addActionListener(listener);
    }

    /**
     * Adds a Listener to the Exit Button
     * @param listener is the action to perform
     */
    public void addExitListener(ActionListener listener) {
        exit.addActionListener(listener);
    }
}
