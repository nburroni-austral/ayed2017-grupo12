package main.KnightTour.GUI.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * The Booth of the starting Knight position selection
 */
public class StartingPositionChoose extends JFrame {

    private JPanel fieldPanel1, fieldPanel2, buttonsPanel, thePanel;
    private JLabel title, row, column;
    private JTextField rowField, columnField;
    private JButton confirm, back;

    public StartingPositionChoose() {

        this.setTitle("Knight Starting Position Selection");
        this.setSize(350, 200);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        title = new JLabel("Select the starting position of the Knight");
        title.setFont(new Font("font", Font.BOLD, 15));
        title.setAlignmentX(CENTER_ALIGNMENT);

        row = new JLabel("Row: ");
        row.setAlignmentX(LEFT_ALIGNMENT);

        column = new JLabel("Column: ");
        column.setAlignmentX(LEFT_ALIGNMENT);

        rowField = new JTextField("", 10);
        rowField.setToolTipText("Type here the starting row");
        rowField.setAlignmentX(CENTER_ALIGNMENT);

        columnField = new JTextField("", 10);
        columnField.setToolTipText("Type here the starting column");
        columnField.setAlignmentX(CENTER_ALIGNMENT);

        confirm = new JButton("Confirm");
        confirm.setToolTipText("Confirm the chosen starting position");
        confirm.setAlignmentX(LEFT_ALIGNMENT);

        back = new JButton("Back");
        back.setToolTipText("Go back to the main window");
        back.setAlignmentX(RIGHT_ALIGNMENT);

        fieldPanel1 = new JPanel();
        fieldPanel1.add(row);
        fieldPanel1.add(Box.createHorizontalStrut(12));
        fieldPanel1.add(rowField);

        fieldPanel2 = new JPanel();
        fieldPanel2.add(column);
        fieldPanel2.add(columnField);

        buttonsPanel = new JPanel();
        buttonsPanel.add(confirm);
        buttonsPanel.add(back);

        thePanel = new JPanel();
        thePanel.setLayout(new BoxLayout(thePanel, BoxLayout.PAGE_AXIS));
        thePanel.add(Box.createVerticalGlue());
        thePanel.add(title);
        thePanel.add(Box.createHorizontalStrut(75));
        thePanel.add(fieldPanel1);
        thePanel.add(Box.createHorizontalStrut(10));
        thePanel.add(fieldPanel2);
        thePanel.add(Box.createHorizontalStrut(20));
        thePanel.add(buttonsPanel);
        thePanel.add(Box.createVerticalGlue());

        this.add(thePanel);
        this.setVisible(false);
    }

    /**
     * Turns the window Visible
     */
    public void showSelf() {
        setVisible(true);
    }

    /**
     * Turns the window Invisible
     */
    public void hideSelf() {
        setVisible(false);
    }

    /**
     * Adds a Listener to the Confirm Button
     * @param listener is the action to perform
     */
    public void addConfirmListener(ActionListener listener) {
        confirm.addActionListener(listener);
    }

    /**
     * Adds a Listener to the Back Button
     * @param listener is the action to perform
     */
    public void addBackListener(ActionListener listener) {
        back.addActionListener(listener);
    }

    public String getRowEntered() {
        return rowField.getText();
    }

    public String getColumnEntered() {
        return columnField.getText();
    }
}
