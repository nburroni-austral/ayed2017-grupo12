package main.KnightTour.GUI.View;

import main.KnightTour.Logic.KnightTour;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * The Booth that shows the Knight Tour in a Board
 */
public class KnightTourBoard extends JFrame {

    private JPanel mainPanel, boardPanel, rowsPanel, columnsPanel, middlePanel;
    private JPanel[][] blocks;
    private JLabel[][] knightImages;
    private JButton next, exit;
    private JLabel a, b, c, d, e, f, g, h, c1, c2, c3, c4, c5, c6, c7, c8;
    private KnightTour knightTour;
    private int[][] position;

    public KnightTourBoard() {

        this.setTitle("Knight Tour");
        this.setSize(600, 600);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        next = new JButton("Next");
        next.setToolTipText("Make next move");
        next.setAlignmentX(CENTER_ALIGNMENT);

        exit = new JButton("Exit");
        exit.setToolTipText("Exit to Starting Position Choose");
        exit.setAlignmentX(CENTER_ALIGNMENT);

        mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(next, BorderLayout.LINE_END);
        mainPanel.add(exit, BorderLayout.PAGE_END);

        boardPanel = new JPanel(new GridLayout(8, 8));
        boardPanel.setBorder(new LineBorder(Color.black));

        blocks = new JPanel[8][8];

        knightImages = new JLabel[8][8];

        Color black = Color.BLACK;
        Color white = Color.WHITE;

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (i % 2 == 0) {
                    if (j % 2 == 0) {
                        blocks[i][j] = new JPanel();
                        blocks[i][j].setBackground(white);
                        boardPanel.add(blocks[i][j]);
                    } else {
                        blocks[i][j] = new JPanel();
                        blocks[i][j].setBackground(black);
                        boardPanel.add(blocks[i][j]);
                    }
                } else if(j % 2 == 0) {
                    blocks[i][j] = new JPanel();
                    blocks[i][j].setBackground(black);
                    boardPanel.add(blocks[i][j]);
                } else {
                    blocks[i][j] = new JPanel();
                    blocks[i][j].setBackground(white);
                    boardPanel.add(blocks[i][j]);
                }
                knightImages[i][j] = new JLabel();
                knightImages[i][j].setIcon(new ImageIcon(KnightTourBoard.class.getResource("/main/KnightTour/GUI/View/Images/knight.png")));
                knightImages[i][j].setVisible(false);
                blocks[i][j].add(knightImages[i][j]);
            }
        }

        rowsPanel = new JPanel(new GridLayout(8, 1));

        a = new JLabel("  1  ");
        b = new JLabel("  2  ");
        c = new JLabel("  3  ");
        d = new JLabel("  4  ");
        e = new JLabel("  5  ");
        f = new JLabel("  6  ");
        g = new JLabel("  7  ");
        h = new JLabel("  8  ");

        rowsPanel.add(a);
        rowsPanel.add(b);
        rowsPanel.add(c);
        rowsPanel.add(d);
        rowsPanel.add(e);
        rowsPanel.add(f);
        rowsPanel.add(g);
        rowsPanel.add(h);

        columnsPanel = new JPanel(new GridLayout(1, 8));

        c1 = new JLabel("A", SwingConstants.CENTER);
        c2 = new JLabel("B", SwingConstants.CENTER);
        c3 = new JLabel("C", SwingConstants.CENTER);
        c4 = new JLabel("D", SwingConstants.CENTER);
        c5 = new JLabel("E", SwingConstants.CENTER);
        c6 = new JLabel("F", SwingConstants.CENTER);
        c7 = new JLabel("G", SwingConstants.CENTER);
        c8 = new JLabel("H", SwingConstants.CENTER);

        columnsPanel.add(c1);
        columnsPanel.add(c2);
        columnsPanel.add(c3);
        columnsPanel.add(c4);
        columnsPanel.add(c5);
        columnsPanel.add(c6);
        columnsPanel.add(c7);
        columnsPanel.add(c8);

        middlePanel = new JPanel(new BorderLayout());
        middlePanel.add(rowsPanel, BorderLayout.LINE_START);
        middlePanel.add(columnsPanel, BorderLayout.PAGE_END);
        middlePanel.add(boardPanel, BorderLayout.CENTER);

        mainPanel.add(middlePanel, BorderLayout.CENTER);

        knightTour = new KnightTour();

        this.add(mainPanel);
        this.setVisible(false);
    }

    /**
     * Turns the window Visible
     */
    public void showSelf() {
        setVisible(true);
    }

    /**
     * Turns the window Invisible
     */
    public void hideSelf() {
        setVisible(false);
    }

    /**
     * Adds a Listener to the Next Button
     * @param listener is the action to perform
     */
    public void addNextListener(ActionListener listener) {
        next.addActionListener(listener);
    }

    /**
     * Adds a Listener to the Exit Button
     * @param listener is the action to perform
     */
    public void addExitListener(ActionListener listener) {
        exit.addActionListener(listener);
    }

    public KnightTour getKnightTour() {
        return knightTour;
    }

    public JLabel[][] getKnightImages() {
        return knightImages;
    }
}
