package main.KnightTour.GUI.View;

import javax.swing.*;

public class ErrorWindow extends JOptionPane{

    public ErrorWindow(String message, JFrame parent) {
        showMessageDialog(parent, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
