package main.KnightTour.Logic;

import struct.impl.DynamicStack;

/**
 * The KnightTour class contains the logic of the program
 */
public class KnightTour {

    private int[][] grid;
    private int moves;
    private boolean knightSetting;
    private int knightRow, knightColumn;
    private int loops = 0;
    private DynamicStack<Integer> rowsStack1, columnsStack1, rowsStack2, columnsStack2,
                                  rowsStack3, columnsStack3, rowsStack4, columnsStack4;

    /**
     * Creates a Knight Tour
     */
    public KnightTour() {
        grid = new int[8][8];
        moves = 4;
        knightSetting = true;
        rowsStack1 = new DynamicStack<>();
        columnsStack1 = new DynamicStack<>();
        rowsStack2 = new DynamicStack<>();
        columnsStack2 = new DynamicStack<>();
        rowsStack3 = new DynamicStack<>();
        columnsStack3 = new DynamicStack<>();
        rowsStack4 = new DynamicStack<>();
        columnsStack4 = new DynamicStack<>();
    }

    public int[][] getGrid() {
        return grid;
    }

    /*
     If a block contains the value 1, it means that the Knight is currently there, and if it contains -1, it is
     because the Knight has been there at some point
     */

    /**
     * Sets the knight to the starting position
     * @param row    is the desired number of row to locate the knight
     * @param column is the desired number of column to locate the knight
     */
    public void setKnight(int row, int column) {
        if (row >= 1 && row <= 8 && column >= 1 && column <= 8 && knightSetting) {
            grid[row-1][column-1] = 1;
            knightRow = row;
            knightColumn = column;
            knightSetting = false;
        }
    }

    /**
     * Moves the Knight one block up, and two to the right
     */
    public void option1() {
        if (option1Checking()) {
            grid[knightRow-1][knightColumn-1] = -1;
            option1Checking();
            grid[knightRow-1][knightColumn-1] = 1;
            moves--;
        }
    }

    /**
     * Checks if there is an available block to move, going one up, and two to the right
     * @return true if the block is available, false otherwise
     */
    public boolean option1Checking() {
        if ((knightRow + 1) >= 1 && (knightRow + 1) <= 8 && (knightColumn + 2) >= 1 && (knightColumn + 2) <= 8 && moves >= 1 && grid[knightRow][knightColumn + 1] != -1) {
            knightRow = knightRow + 1;
            knightColumn = knightColumn + 2;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the values of the row and column of the Knight, that was changed during option1Checking, back to its current position
     */
    public void option1GoBack() {
        knightRow = knightRow - 1;
        knightColumn = knightColumn - 2;
    }

    /**
     * Moves the Knight one block up, and two to the left
     */
    public void option2() {
        if (option2Checking()) {
            grid[knightRow-1][knightColumn-1] = -1;
            option2Checking();
            grid[knightRow-1][knightColumn-1] = 1;
            moves--;
        }
    }

    /**
     *Checks if there is an available block to move, going one up, and two to the left
     * @return true if the block is available, false otherwise
     */
    public boolean option2Checking() {
        if ((knightRow + 1) >= 1 && (knightRow + 1) <= 8 && (knightColumn - 2) >= 1 && (knightColumn - 2) <= 8 && moves >= 1 && grid[knightRow][knightColumn - 3] != -1) {
            knightRow = knightRow + 1;
            knightColumn = knightColumn - 2;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the values of the row and column of the Knight, that was changed during option2Checking, back to its current position
     */
    public void option2GoBack() {
        knightRow = knightRow - 1;
        knightColumn = knightColumn + 2;
    }

    /**
     * Moves the Knight one block down, and two to the right
     */
    public void option3() {
        if (option3Checking()) {
            grid[knightRow-1][knightColumn-1] = -1;
            option3Checking();
            grid[knightRow-1][knightColumn-1] = 1;
            moves--;
        }
    }

    /**
     * Checks if there is an available block to move, going one down, and two to the right
     * @return true if the block is available, false otherwise
     */
    public boolean option3Checking() {
        if ((knightRow - 1) >= 1 && (knightRow - 1) <= 8 && (knightColumn + 2) >= 1 && (knightColumn + 2) <= 8 && moves >= 1 && grid[knightRow - 2][knightColumn + 1] != -1) {
            knightRow = knightRow - 1;
            knightColumn = knightColumn + 2;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the values of the row and column of the Knight, that was changed during option3Checking, back to its current position
     */
    public void option3GoBack() {
        knightRow = knightRow + 1;
        knightColumn = knightColumn - 2;
    }

    /**
     * Moves the Knight one block down, and two to the right
     */
    public void option4() {
        if (option4Checking()) {
            grid[knightRow-1][knightColumn-1] = -1;
            option4Checking();
            grid[knightRow-1][knightColumn-1] = 1;
            moves--;
        }
    }

    /**
     * Checks if there is an available block to move, going one down, and two to the left
     * @return true if the block is available, false otherwise
     */
    public boolean option4Checking() {
        if ((knightRow - 1) >= 1 && (knightRow - 1) <= 8 && (knightColumn - 2) >= 1 && (knightColumn - 2) <= 8 && moves >= 1 && grid[knightRow - 2][knightColumn - 3] != -1) {
            knightRow = knightRow - 1;
            knightColumn = knightColumn - 2;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the values of the row and column of the Knight, that was changed during option4Checking, back to its current position
     */
    public void option4GoBack() {
        knightRow = knightRow + 1;
        knightColumn = knightColumn + 2;
    }

    /**
     * Moves the Knight two blocks up, and one to the right
     */
    public void option5() {
        if (option5Checking()) {
            grid[knightRow-1][knightColumn-1] = -1;
            option5Checking();
            grid[knightRow-1][knightColumn-1] = 1;
            moves--;
        }
    }

    /**
     * Checks if there is an available block to move, going two up, and one to the right
     * @return true if the block is available, false otherwise
     */
    public boolean option5Checking() {
        if ((knightRow + 2) >= 1 && (knightRow + 2) <= 8 && (knightColumn + 1) >= 1 && (knightColumn + 1) <= 8 && moves >= 1 && grid[knightRow + 1][knightColumn] != -1) {
            knightRow = knightRow + 2;
            knightColumn = knightColumn + 1;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the values of the row and column of the Knight, that was changed during option5Checking, back to its current position
     */
    public void option5GoBack() {
        knightRow = knightRow - 2;
        knightColumn = knightColumn - 1;
    }

    /**
     * Moves the Knight two blocks up, and one to the left
     */
    public void option6() {
        if (option6Checking()) {
            grid[knightRow-1][knightColumn-1] = -1;
            option6Checking();
            grid[knightRow-1][knightColumn-1] = 1;
            moves--;
        }
    }

    /**
     * Checks if there is an available block to move, going two up, and one to the left
     * @return true if the block is available, false otherwise
     */
    public boolean option6Checking() {
        if ((knightRow + 2) >= 1 && (knightRow + 2) <= 8 && (knightColumn - 1) >= 1 && (knightColumn - 1) <= 8 && moves >= 1 && grid[knightRow + 1][knightColumn - 2] != -1) {
            knightRow = knightRow + 2;
            knightColumn = knightColumn - 1;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the values of the row and column of the Knight, that was changed during option6Checking, back to its current position
     */
    public void option6GoBack() {
        knightRow = knightRow - 2;
        knightColumn = knightColumn + 1;
    }

    /**
     * Moves the Knight two blocks down, and one to the right
     */
    public void option7() {
        if (option7Checking()) {
            grid[knightRow-1][knightColumn-1] = -1;
            option7Checking();
            grid[knightRow-1][knightColumn-1] = 1;
            moves--;
        }
    }

    /**
     * Checks if there is an available block to move, going two down, and one to the right
     * @return true if the block is available, false otherwise
     */
    public boolean option7Checking() {
        if ((knightRow - 2) >= 1 && (knightRow - 2) <= 8 && (knightColumn + 1) >= 1 && (knightColumn + 1) <= 8 && moves >= 1 && grid[knightRow - 3][knightColumn] != -1) {
            knightRow = knightRow - 2;
            knightColumn = knightColumn + 1;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the values of the row and column of the Knight, that was changed during option7Checking, back to its current position
     */
    public void option7GoBack() {
        knightRow = knightRow + 2;
        knightColumn = knightColumn - 1;
    }

    /**
     * Moves the Knight two blocks down, and one to the left
     */
    public void option8() {
        if (option8Checking()) {
            grid[knightRow-1][knightColumn-1] = -1;
            option8Checking();
            grid[knightRow-1][knightColumn-1] = 1;
            moves--;
        }
    }

    /**
     * Checks if there is an available block to move, going two down, and one to the left
     * @return true if the block is available, false otherwise
     */
    public boolean option8Checking() {
        if ((knightRow - 2) >= 1 && (knightRow - 2) <= 8 && (knightColumn - 1) >= 1 && (knightColumn - 1) <= 8 && moves >= 1 && grid[knightRow - 3][knightColumn - 2] != -1) {
            knightRow = knightRow - 2;
            knightColumn = knightColumn - 1;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the values of the row and column of the Knight, that was changed during option8Checking, back to its current position
     */
    public void option8GoBack() {
        knightRow = knightRow + 2;
        knightColumn = knightColumn + 1;
    }

    /**
     * Moves the Knight to any available position
     */
    public void move() {
        if (option1Checking()) {
            option1();
        } else if (option2Checking()) {
            option2();
        } else if (option3Checking()) {
            option3();
        } else if (option4Checking()) {
            option4();
        } else if (option5Checking()) {
            option5();
        } else if (option6Checking()) {
            option6();
        } else if (option7Checking()) {
            option7();
        } else if (option8Checking()) {
            option8();
        }
    }

    /**
     * Moves the Knight to an available position
     */
    public void moveKnight() {
        if (loops == 0) {
            stackFilling(rowsStack1, columnsStack1);
        }
        loops++;
        if (loops == 1) {
            move();
            stackFilling(rowsStack2, columnsStack2);
        }
        if (loops == 2) {
            move();
            stackFilling(rowsStack3, columnsStack3);
        }
        if (loops == 3) {
            move();
            stackFilling(rowsStack4, columnsStack4);
        }
        if (loops == 4) {
            move();
        }
        if (loops == 5) {
            rowsStack4.pop();
        }
        if (loops == 6) {
            rowsStack4.peek();
            columnsStack4.peek();
        }
        //TODO
    }

    /**
     * Fills the stacks with the available positions to move the Knight
     * @param rowsStack the row stack to fill
     * @param columnsStack the column stack to fill
     */
    public void stackFilling(DynamicStack<Integer> rowsStack, DynamicStack<Integer> columnsStack) {
        if (rowsStack.isEmpty() && columnsStack.isEmpty()) {
            if (option1Checking()) {
                option1Checking();
                rowsStack.push(knightRow);
                columnsStack.push(knightColumn);
                option1GoBack();
            }
            if (option2Checking()) {
                option2Checking();
                rowsStack.push(knightRow);
                columnsStack.push(knightColumn);
                option2GoBack();
            }
            if (option3Checking()) {
                option3Checking();
                rowsStack.push(knightRow);
                columnsStack.push(knightColumn);
                option3GoBack();
            }
            if (option4Checking()) {
                option4Checking();
                rowsStack.push(knightRow);
                columnsStack.push(knightColumn);
                option4GoBack();
            }
            if (option5Checking()) {
                option5Checking();
                rowsStack.push(knightRow);
                columnsStack.push(knightColumn);
                option5GoBack();
            }
            if (option6Checking()) {
                option6Checking();
                rowsStack.push(knightRow);
                columnsStack.push(knightColumn);
                option6GoBack();
            }
            if (option7Checking()) {
                option7Checking();
                rowsStack.push(knightRow);
                columnsStack.push(knightColumn);
                option7GoBack();
            }
            if (option8Checking()) {
                option8Checking();
                rowsStack.push(knightRow);
                columnsStack.push(knightColumn);
                option8GoBack();
            }
        }
    }
}
