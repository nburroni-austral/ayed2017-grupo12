package main.tpTrees;
public class Node2<T extends Comparable<T>> extends Node<T>  {
    T data1;
    private Node<T> left;
    private Node<T> right;

    public Node2() {
        type = 2;
    }


    public Node3<T> convertTo3(T t){
        Node3<T> node3 = new Node3<>();
        node3.setFather(this.getFather());
        node3.setLeft(left);
        node3.setRight(right);
        if(t.compareTo(data1)>0){
            node3.data2 = t;
            node3.data1 = data1;
        }else{
            node3.data2 = data1;
            node3.data1 = t;
        }
        if(getFather()!=null)getFather().setChild(t,node3);
        return node3;
    }

    public Node<T> search(T t) {
        int comparedWithData1 = t.compareTo(data1);
        if(this.isLeaf()) return this;
        else{
            if(comparedWithData1>0){
                return right.search(t);
            }else{
                return left.search(t);
            }
        }
    }

    public boolean isLeaf() {
        if(left ==null && right == null) return true;
        else return false;
    }

    public Node<T> insert(T t) {
        if(data1==null){
            data1 = t;
            return this;
        }else{
            return convertTo3(t);
        }
    }

    public void setChild(T t,Node<T> child) {
        int comparedWithData1 = t.compareTo(data1);
        if(comparedWithData1>0) this.setRight(child);
        else this.setLeft(child);
    }

    public void print() {
        System.out.println("D1: "+data1);
        if(left!=null) left.print();
        if(right!=null) right.print();
    }

    public Object[] getData() {
        Object[] array = new Object[1];
        array[0] = data1;
        for (int i = 0; i < type-1; i++){
            array[i] = (T) array[i];
        }
        return array;
    }

    public Node<T> getLeft() {
        return left;
    }

    public void setLeft(Node<T> left) {
        this.left = left;
        if(left!=null){
            left.setFather(this);
        }
    }

    public Node<T> getRight() {
        return right;
    }

    public void setRight(Node<T> right) {
        this.right = right;
        if(right!=null){
            right.setFather(this);
        }
    }

}