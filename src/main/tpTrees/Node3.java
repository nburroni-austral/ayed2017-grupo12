package main.tpTrees;

public class Node3 <T extends Comparable<T>> extends Node2<T>{
    T data2;
    private Node<T> center1;

    public Node3() {
        type = 3;

    }

    public Node<T> getCenter1() {
        return center1;
    }

    public void setCenter1(Node<T> center1) {
        if(center1!=null){
            center1.setFather(this);
        }
        this.center1 = center1;
    }

    public Node4<T> convertTo4(T t){
        Node4<T> node4 = new Node4<>();
        node4.setFather(getFather());
        node4.setLeft(getLeft());
        node4.setCenter1(center1);
        node4.setCenter2(center1);
        //tengo que ver aca cual realmente se asigna, si center1 o center2;
        node4.setRight(getRight());
        int comparedWithData1 = t.compareTo(data1);
        int comparedWithData2 = t.compareTo(data2);
        if(comparedWithData1<0){
            node4.data3 = data2;
            node4.data2 = data1;
            node4.data1 = t;
        }else{
            if(comparedWithData2<0){
                node4.data1 = data1;
                node4.data2 = t;
                node4.data3 = data2;
            }else{
                node4.data1= data1;
                node4.data2= data2;
                node4.data3= t;
            }
        }
        if(getFather()!=null) getFather().setChild(t,node4);
        return node4;
    }

    public Node<T> search(T t) {
        int comparedWithData1 = t.compareTo(data1);
        int comparedWithData2 = t.compareTo(data2);
        if(this.isLeaf()) return this;
        else{
            if(comparedWithData1<0) return getLeft().search(t);
            else{
                if(comparedWithData2<0) return getCenter1().search(t);
                else{
                    return getRight().search(t);
                }
            }
        }
    }

    public void print() {
        System.out.println("D1: "+data1 + " D2: "+data2);
        if(getLeft()!=null) getLeft().print();
        if(center1!=null) center1.print();
        if(getRight()!=null) getRight().print();
    }

    public Node<T> insert(T t) {
        return convertTo4(t);
    }

    public void setChild(T t, Node<T> child) {
        int comparedWithData1 = t.compareTo(data1);
        int comparedWithData2 = t.compareTo(data2);
        if(comparedWithData1<0) setLeft(child);
        else{
            if(comparedWithData2<0) setCenter1(child);
            else{
                setRight(child);
            }
        }
    }
    public Object[] getData() {
        Object[] array = new Object[2];
        array[0] = data1;
        array[1] = data2;
        for (int i = 0; i < type-1; i++){
            array[i] = (T) array[i];
        }
        return array;
    }
}
