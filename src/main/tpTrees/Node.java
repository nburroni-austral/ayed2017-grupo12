package main.tpTrees;

public abstract class Node <T extends Comparable<T>> {
    private Node<T> father;
    public int type;
    int x;
    int y;
    public abstract Node<T> search(T t);
    public abstract boolean isLeaf();
    public abstract Node<T> insert(T t);
    public abstract void setChild(T t,Node<T> child);
    public abstract void print();
    public Node<T> getFather() {
        return father;
    }
    public abstract Object[] getData();
    public void setFather(Node<T> father) {
        this.father = father;
    }
}