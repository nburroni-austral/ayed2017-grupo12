package main.tpTrees;

public class Tree234<T extends Comparable<T>>  {

    private Node<T> root;

    public Tree234(T t) {
        Node2<T> node2 = new Node2<T>();
        node2.data1 = t;
        root = node2;
        root.x = 400;
    }

    public Node<T> getRoot() {
        return root;
    }

    public void insert(T t){
        Node<T> node = root.search(t);
        node = node.insert(t);
        Node<T> father = node.getFather();
        if(father!=null){
            while(father.getFather() != null){
                father = father.getFather();
            }
            root = father;
        }
        else{
            root = node;
        }
    }

    public void print() {
        root.print();
    }
}
