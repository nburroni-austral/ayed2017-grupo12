package main.tp4.Simulation.Models;

/**
 * OptionB consists in having a different queue for each cashier
 */
public class OptionB extends Abstract {

    int actualClients = cashier1Queue.size() + cashier2Queue.size() + cashier3Queue.size();

    public OptionB() {
    }

    public void simulation() {
        for (int i = 0; i < totalTime; i++) {
            addClientsToTheQueues();
            addClientsToTheCashiers();
            openedTime++;
        }
        while (!cashier1Queue.isEmpty() || !cashier2Queue.isEmpty() || !cashier3Queue.isEmpty()) {
            addClientsToTheCashiers();
            openedTime++;
            timeOpenedAfterClosing++;
        }
    }

    @SuppressWarnings("Duplicates")
    public void addClientsToTheQueues() {

        boolean queueChooseProb1 = random.nextInt(2)==0;    // %50 of chances of choosing any of the two available queues
        boolean queueChooseProb2a = random.nextInt(3)==0;   // %33.33 of chances of choosing one of the three available queues
        boolean queueChooseProb2b = random.nextInt(3)==0;   // (idem)

        if (openedTime % 90 == 0) {
            if (cashier1Queue.isEmpty() && cashier2Queue.isEmpty() && cashier3Queue.isEmpty()) {
                if (queueChooseProb2a) {
                    applyConditions(cashier1Queue, actualClients);
                } else if (queueChooseProb2b) {
                    applyConditions(cashier2Queue, actualClients);
                } else {
                    applyConditions(cashier3Queue, actualClients);
                }
            } else if (cashier1Queue.isEmpty() && cashier2Queue.isEmpty() && !cashier3Queue.isEmpty()) {
                if (queueChooseProb1) {
                    applyConditions(cashier1Queue, actualClients);
                } else {
                    applyConditions(cashier2Queue, actualClients);
                }
            } else if (cashier1Queue.isEmpty() && !cashier2Queue.isEmpty() && cashier3Queue.isEmpty()) {
                if (queueChooseProb1) {
                    applyConditions(cashier1Queue, actualClients);
                } else {
                    applyConditions(cashier3Queue, actualClients);
                }
            } else if (!cashier1Queue.isEmpty() && cashier2Queue.isEmpty() && cashier3Queue.isEmpty()) {
                if (queueChooseProb1) {
                    applyConditions(cashier2Queue, actualClients);
                } else {
                    applyConditions(cashier3Queue, actualClients);
                }
            } else if (cashier1Queue.isEmpty() && !cashier2Queue.isEmpty() && !cashier3Queue.isEmpty()) {
                applyConditions(cashier1Queue, actualClients);
            } else if (!cashier1Queue.isEmpty() && cashier2Queue.isEmpty() && !cashier3Queue.isEmpty()) {
                applyConditions(cashier2Queue, actualClients);
            } else if (!cashier1Queue.isEmpty() && !cashier2Queue.isEmpty() && cashier3Queue.isEmpty()) {
                applyConditions(cashier3Queue, actualClients);
            } else if (!cashier1Queue.isEmpty() && !cashier2Queue.isEmpty() && !cashier3Queue.isEmpty()) {
                int a = cashier1Queue.length();
                int b = cashier2Queue.length();
                int c = cashier3Queue.length();
                if (a < b && a < c) {
                    applyConditions(cashier1Queue, actualClients);
                } else if (b < a && b < c) {
                    applyConditions(cashier2Queue, actualClients);
                } else if (c < a && c < b) {
                    applyConditions(cashier3Queue, actualClients);
                } else if (a == b && a < c) {
                    if (queueChooseProb1) {
                        applyConditions(cashier1Queue, actualClients);
                    } else {
                        applyConditions(cashier2Queue, actualClients);
                    }
                } else if (a == c && a < b) {
                    if (queueChooseProb1) {
                        applyConditions(cashier1Queue, actualClients);
                    } else {
                        applyConditions(cashier3Queue, actualClients);
                    }
                } else if (b == c && b < a) {
                    if (queueChooseProb1) {
                        applyConditions(cashier2Queue, actualClients);
                    } else {
                        applyConditions(cashier3Queue, actualClients);
                    }
                } else if (a == b && a == c) {
                    if (queueChooseProb2a) {
                        applyConditions(cashier1Queue, actualClients);
                    } else if (queueChooseProb2b) {
                        applyConditions(cashier2Queue, actualClients);
                    } else {
                        applyConditions(cashier3Queue, actualClients);
                    }
                }
            }
        }
    }

    public void addClientsToTheCashiers() {

        int cashier1TimeProb = random.nextInt(cashier1MaxTime - cashier1MinTime) + cashier1MinTime;
        int cashier2TimeProb = random.nextInt(cashier2MaxTime - cashier2MinTime) + cashier2MinTime;
        int cashier3TimeProb = random.nextInt(cashier3MaxTime - cashier3MinTime) + cashier3MinTime;

        if (!cashier1Queue.isEmpty() && (openedTime % cashier1TimeProb) == 0) {
            cashier1.setClient(cashier1Queue.dequeue());
            clientsAttended++;
        }
        if (!cashier2Queue.isEmpty() && (openedTime % cashier2TimeProb) == 0) {
            cashier2.setClient(cashier2Queue.dequeue());
            clientsAttended++;
        }
        if (!cashier3Queue.isEmpty() && (openedTime % cashier3TimeProb) == 0) {
            cashier3.setClient(cashier3Queue.dequeue());
            clientsAttended++;
        }
    }
}
