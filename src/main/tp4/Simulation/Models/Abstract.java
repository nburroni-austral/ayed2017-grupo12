package main.tp4.Simulation.Models;

import main.tp4.Simulation.Cashiers.Cashier;
import struct.impl.DynamicQueue;

import java.util.Random;

public abstract class Abstract {

    /*
    The time will be evaluated in seconds
     */

    // VARIABLES
    Cashier cashier1 = new Cashier(30, 90);
    Cashier cashier2 = new Cashier(30, 120);
    Cashier cashier3 = new Cashier(30, 150);

    DynamicQueue<Integer> clientsQueue = new DynamicQueue<>();

    DynamicQueue<Integer> cashier1Queue = new DynamicQueue<>();
    DynamicQueue<Integer> cashier2Queue = new DynamicQueue<>();
    DynamicQueue<Integer> cashier3Queue = new DynamicQueue<>();

    int clients = 0;
    long openedTime = 0;
    long totalTime = 18000;  // 18000 seconds equals to 5 hours, the time the bank is opened per day
    int clientsAttended = 0;
    int timeOpenedAfterClosing;

    Random random = new Random();

    int minClientsAmount = 3;   // estimated minor quantity of clients that arrive to the bank per 90 seconds
    int maxClientsAmount = 7;   // estimated maximum quantity of clients that arrive to the bank per 90 seconds

    int cashier1MinTime = cashier1.getMinTime();
    int cashier1MaxTime = cashier1.getMaxTime() + 1;

    int cashier2MinTime = cashier2.getMinTime();
    int cashier2MaxTime = cashier2.getMaxTime() + 1;

    int cashier3MinTime = cashier3.getMinTime();
    int cashier3MaxTime = cashier3.getMaxTime() + 1;


    // METHODS
    public void applyConditions(DynamicQueue<Integer> queue, int actualClients) {

        int clientsAmount = random.nextInt(maxClientsAmount - minClientsAmount) + minClientsAmount;
        boolean rejectProb1 = random.nextInt(4)==0;   // %25 of rejection
        boolean rejectProb2 = random.nextInt(2)==0;   // %50 of rejection

        for (int i = 0; i < clientsAmount; i++) {
            if (actualClients <= 3) {
                queue.enqueue(clients++);
            } else if (actualClients > 4 && actualClients <= 8 && !rejectProb1) {
                queue.enqueue(clients++);
            } else if (actualClients > 9 && !rejectProb2) {
                queue.enqueue(clients++);
            }
        }
    }

    public int getClientsAttended() {
        return clientsAttended;
    }

    public int getTimeOpenedAfterClosing() {
        return timeOpenedAfterClosing;
    }
}
