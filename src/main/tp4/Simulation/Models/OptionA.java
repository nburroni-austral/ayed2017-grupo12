package main.tp4.Simulation.Models;

/**
 * OptionA consists in having only one queue in common for all cashiers
 */
public class OptionA extends Abstract {

    int actualClients = clientsQueue.size();

    public OptionA() {
    }

    public void simulation() {
        for (int i = 0; i < totalTime; i++) {
            addClientsToTheQueue();
            addClientsToTheCashiers();
            openedTime++;
        }
        while (!clientsQueue.isEmpty()) {
            addClientsToTheCashiers();
            openedTime++;
            timeOpenedAfterClosing++;
        }
    }

    public void addClientsToTheQueue() {
        if (openedTime == 0 || openedTime % 90 == 0) {
            applyConditions(clientsQueue, actualClients);
        }
    }

    @SuppressWarnings("Duplicates")
    public void addClientsToTheCashiers() {

        int cashier1TimeProb = random.nextInt(cashier1MaxTime - cashier1MinTime) + cashier1MinTime;
        int cashier2TimeProb = random.nextInt(cashier2MaxTime - cashier2MinTime) + cashier2MinTime;
        int cashier3TimeProb = random.nextInt(cashier3MaxTime - cashier3MinTime) + cashier3MinTime;

        boolean cashierChooseProb1 = random.nextInt(2)==0;    // %50 of chances of choosing any of the two available cashiers
        boolean cashierChooseProb2a = random.nextInt(3)==0;   // %33.33 of chances of choosing one of the three available cashiers
        boolean cashierChooseProb2b = random.nextInt(3)==0;   // (idem)

        boolean option1 = openedTime % cashier1TimeProb == 0;
        boolean option2 = openedTime % cashier2TimeProb == 0;
        boolean option3 = openedTime % cashier3TimeProb == 0;
        if (!clientsQueue.isEmpty()) {
            if (option1 && option2 && !option3) {
                if (cashierChooseProb1) {
                    cashier1.setClient(clientsQueue.dequeue());
                } else {
                    cashier2.setClient(clientsQueue.dequeue());
                }
                clientsAttended++;
            }
            if (option1 && !option2 && option3) {
                if (cashierChooseProb1) {
                    cashier1.setClient(clientsQueue.dequeue());
                } else {
                    cashier3.setClient(clientsQueue.dequeue());
                }
                clientsAttended++;
            }
            if (!option1 && option2 && option3) {
                if (cashierChooseProb1) {
                    cashier2.setClient(clientsQueue.dequeue());
                } else {
                    cashier3.setClient(clientsQueue.dequeue());
                }
                clientsAttended++;
            }
            if (option1 && !option2 && !option3) {
                cashier1.setClient(clientsQueue.dequeue());
                clientsAttended++;
            }
            if (!option1 && option2 && !option3) {
                cashier2.setClient(clientsQueue.dequeue());
                clientsAttended++;
            }
            if (!option1 && !option2 && option3) {
                cashier3.setClient(clientsQueue.dequeue());
                clientsAttended++;
            }
            if (option1 && option2 && option3) {
                if (cashierChooseProb2a) {
                    cashier1.setClient(clientsQueue.dequeue());
                } else if (cashierChooseProb2b) {
                    cashier2.setClient(clientsQueue.dequeue());
                } else {
                    cashier3.setClient(clientsQueue.dequeue());
                }
                clientsAttended++;
            }
            if (!option1 && !option2 && !option3) {
                // do nothing (wait until a cashier becomes available)
            }
        }
    }
}
