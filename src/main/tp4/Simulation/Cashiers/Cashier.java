package main.tp4.Simulation.Cashiers;

public class Cashier {

    private int minTime;   // evaluated in seconds
    private int maxTime;   // evaluated in seconds
    private int clients;

    public Cashier(int minTime, int maxTime) {
        this.minTime = minTime;
        this.maxTime = maxTime;
    }

    public int getMinTime() {
        return minTime;
    }

    public int getMaxTime() {
        return maxTime;
    }

    public void setClient(int clients) {
        this.clients = clients;
    }
}
