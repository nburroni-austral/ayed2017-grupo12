package main.tp4.PriorityQueue;

import struct.impl.DynamicQueue;

import java.util.ArrayList;

/***
 * class that administers queues with different priorities
 * @param <T> type of the queues
 */
public class PriorityQueue<T> {
    private ArrayList<DynamicQueue<T>> priorityList;
    private int priorityNumber;

    /***
     * creates a PriorityQueue with a set number of priorities
     * @param priorityNumber the number of priorities
     */
    public PriorityQueue(int priorityNumber){
        priorityList = new ArrayList<>();
        this.priorityNumber = priorityNumber;
        for (int i = 0; i < this.priorityNumber; i++){
            priorityList.add(i,new DynamicQueue<>());
        }
    }

    /***
     * enqueues an element in a specific priority
     * @param t the element
     * @param priority the priority given
     */
    public void enqueue(T t, int priority){
        priorityList.get(1).enqueue(t);
    }

    /***
     * returns the element in the highest priority (1 being the most important)
     * @return the element in the highest priority
     */
    public T dequeue(){
        for (DynamicQueue<T> q : priorityList){
            if (!q.isEmpty())
                return q.dequeue();
        }
        return null;
    }
}