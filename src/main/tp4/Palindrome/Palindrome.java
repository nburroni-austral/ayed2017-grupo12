package main.tp4.Palindrome;

import struct.impl.DynamicQueue;
import struct.impl.DynamicStack;

/**
 * Palindrome class determines if a word or phrase is, in fact, a palindrome.
 */
public class Palindrome {

    private String theString;
    private DynamicStack<Character> theStack;
    private DynamicQueue<Character> theQueue;
    private boolean checking;

    public Palindrome(String theString) {
        this.theString = theString.toLowerCase();
        theStack = new DynamicStack<>();
        theQueue = new DynamicQueue<>();
    }

    public boolean check() {

        // fill the stack and the queue
        for (int i = 0; i < theString.length(); i++) {
            if (theString.charAt(i) != ' ') {
                theStack.push(theString.charAt(i));
                theQueue.enqueue(theString.charAt(i));
            }
        }

        // check if the String is a palindrome
        while (!theStack.isEmpty() && !theQueue.isEmpty()) {
            if (theStack.peek().equals(theQueue.dequeue())) {
                theStack.pop();
                checking = true;
            } else {
                checking = false;
                break;
            }
        }

        return checking;
    }
}
