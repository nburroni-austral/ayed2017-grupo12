package main.Parcial2;

import java.io.File;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the path of the text file to analyze the following example: ");
        System.out.println();
        System.out.println("C:\\Users\\admin\\Desktop\\Test.txt");
        System.out.println();

        String filePath;
        while (true) {
            String theFilePath = scanner.next();
            System.out.println();
            File file = new File(theFilePath);
            if (file.exists() && !file.isDirectory()) {
                filePath = theFilePath;
                break;
            } else {
                System.out.println("The file could not be found. Please, check if the path is correct and try again");
                System.out.println();
            }
        }

        Program program = new Program(filePath);
        double qualityControl = program.qualityCheck();
        System.out.println("%" + qualityControl * 100 + " of all products passed the quality control.");
    }
}
