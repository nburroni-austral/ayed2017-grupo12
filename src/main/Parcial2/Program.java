package main.Parcial2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Program {

    private String fileName;

    public Program(String fileName) {
        this.fileName = fileName;
    }

    public double qualityCheck() {
        double passed = 0;
        double notPassed = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String sCurrentLine = br.readLine();
            while (sCurrentLine != null) {
                if (sCurrentLine.charAt(8) == '1') passed++;
                else  notPassed++;
                sCurrentLine = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return passed / (passed + notPassed);
    }
}
