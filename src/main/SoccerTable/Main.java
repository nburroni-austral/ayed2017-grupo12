package main.SoccerTable;

public class Main {

    public static void main(String[] args) {

        Input input = new Input();
        input.generateInput();
        Simulation simulation = new Simulation(input);
        simulation.generateSimulation();
    }
}
