package main.SoccerTable;

public class Team {

    private String name;
    private int leaguePoints;

    public Team(String name, int leaguePoints) {
        this.name = name;
        this.leaguePoints = leaguePoints;
    }

    public String getName() {
        return name;
    }

    public int getLeaguePoints() {
        return leaguePoints;
    }

    public void setLeaguePoints(int leaguePoints) {   // the value is changed only to evaluate if the team won, tied, or lost
        this.leaguePoints = leaguePoints;
    }
}
