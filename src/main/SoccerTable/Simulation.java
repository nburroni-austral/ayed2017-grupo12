package main.SoccerTable;

public class Simulation {

    private Input input;
    private int quantityOfTeams, quantityOfMatches;

    public Simulation(Input input) {
        this.input = input;
        quantityOfTeams = input.getTeam().length;
        quantityOfMatches = input.getMatches().length;
    }

    @SuppressWarnings("Duplicates")
    public void generateSimulation() {

        for (int i = 0; i < quantityOfMatches; i++) {
            for (int j = 0; j < quantityOfTeams; j++) {
                if (input.getMatches()[i].getLocalTeam().equals(input.getTeam()[j].getName())) {
                    while (input.getTeam()[i].getLeaguePoints() > 0) {
                        if ((input.getTeam()[i].getLeaguePoints() - 3) >= 0) {
                            input.getTeam()[i].setLeaguePoints(input.getTeam()[i].getLeaguePoints() - 3);
                            System.out.print ("1 ");
                        } else if ((input.getTeam()[i].getLeaguePoints() - 1) >= 0) {
                            input.getTeam()[i].setLeaguePoints(input.getTeam()[i].getLeaguePoints() - 1);
                            System.out.print("X ");
                        }
                    }
                } else if (input.getMatches()[i].getVisitorTeam().equals(input.getTeam()[j].getName())) {
                    while (input.getTeam()[i].getLeaguePoints() > 0) {
                        if ((input.getTeam()[i].getLeaguePoints() - 3) >= 0) {
                            input.getTeam()[i].setLeaguePoints(input.getTeam()[i].getLeaguePoints() - 3);
                            System.out.print("2 ");
                        } else if ((input.getTeam()[i].getLeaguePoints() - 1) >= 0) {
                            input.getTeam()[i].setLeaguePoints(input.getTeam()[i].getLeaguePoints() - 1);
                            System.out.print("X ");
                        }
                    }
                }
            }
        }
    }
}
