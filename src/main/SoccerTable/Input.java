package main.SoccerTable;

import java.util.Scanner;

public class Input {

    private Team[] team;
    private Match[] matches;
    private int quantityOfTeams, quantityOfMatches;
    private int cases = 0;
    private boolean canProceed = true;

    public Input() {
    }

    public void generateInput() {

        if (canProceed) {

            Scanner scanner = new Scanner(System.in);

            if (cases <= 20) {

                System.out.println("Enter the quantity of teams in the league: ");
                int teamsNumber = scanner.nextInt();
                if (teamsNumber >= 2 && teamsNumber <= 10) {
                    quantityOfTeams = teamsNumber;
                } else {
                    System.out.println("The Max number of teams is 10, and the Min is 2. Restart the program and start again.");
                    System.exit(1);
                }

                team = new Team[quantityOfTeams];

                System.out.println("Enter the quantity of matches played: ");
                int matchesNumber = scanner.nextInt();
                if (matchesNumber >= 1 && matchesNumber <= 20) {
                    quantityOfMatches = matchesNumber;
                } else {
                    System.out.println("The Max number of matches is 20, and the Min is 1. Restart the program and start again.");
                    System.exit(1);
                }

                matches = new Match[quantityOfMatches];

                String teamName;
                int points;
                for (int i = 0; i < quantityOfTeams; i++) {
                    System.out.println("Team #" + i);
                    System.out.println("Enter the name of a team:  (without spaces)");
                    teamName = scanner.next();
                    System.out.println("Enter its total league points: ");
                    points = scanner.nextInt();
                    if (! (points >= 0 && points <= quantityOfMatches * 3)) {
                        System.out.println("Invalid score. Restart the program and start again.");
                        System.exit(1);
                    }
                    team[i] = new Team(teamName, points);
                }

                String teamName1;
                String teamName2;
                boolean existsName1 = false;
                boolean existsName2 = false;
                for (int i = 0; i < quantityOfMatches; i++) {
                    System.out.println("Match #" + i);
                    System.out.println("Enter the name of the local team:  (without spaces)");
                    teamName1 = scanner.next();
                    for (int j = 0; j < quantityOfTeams; j++) {
                        if (team[j].getName().equals(teamName1)) {
                            existsName1 = true;
                            break;
                        }
                    }
                    if (!existsName1) {
                        System.out.println("The team is not in the league. Restart the program and start again.");
                        System.exit(1);
                    }
                    System.out.println("Enter the name of the visitor team:  (without spaces)");
                    teamName2 = scanner.next();
                    for (int j = 0; j < quantityOfTeams; j++) {
                        if (team[j].getName().equals(teamName2) && !teamName2.equals(teamName1)) {
                            existsName2 = true;
                            break;
                        }
                    }
                    if (!existsName2) {
                        System.out.println("The team is not in the league. Restart the program and start again.");
                        System.exit(1);
                    }
                    matches[i] = new Match(teamName1, teamName2);
                }
            }

            System.out.println("If you want to evaluate another case, type 0. Otherwise, type -1 to exit.");
            int action = scanner.nextInt();
            if (action == 0) {
                System.out.println("");
                cases++;
                generateInput();
            } else if (action == -1) {
                canProceed = false;
            }
        }
    }

    public Team[] getTeam() {
        return team;
    }

    public Match[] getMatches() {
        return matches;
    }
}
