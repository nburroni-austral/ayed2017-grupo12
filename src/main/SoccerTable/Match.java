package main.SoccerTable;

public class Match {

    private String localTeam;
    private String visitorTeam;

    public Match(String localTeam, String visitorTeam) {
        this.localTeam = localTeam;
        this.visitorTeam = visitorTeam;
    }

    public String getLocalTeam() {
        return localTeam;
    }

    public String getVisitorTeam() {
        return visitorTeam;
    }
}
