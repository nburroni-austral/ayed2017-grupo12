package main.tp10.Ejercicio1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Program {

    private String fileName;

    public Program(String fileName) {
        this.fileName = fileName;
    }

    public int countCharacters() {
        int charactersQuantity = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String sCurrentLine = br.readLine();
            while (sCurrentLine != null) {
                for (int i = 0; i < sCurrentLine.length(); i++) {
                    if (sCurrentLine.charAt(i) != ' ') {
                        charactersQuantity++;
                    }
                }
                sCurrentLine = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return charactersQuantity;
    }

    public int countLines() {
        int linesQuantity = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String sCurrentLine = br.readLine();
            while (sCurrentLine != null) {
                linesQuantity++;
                sCurrentLine = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return linesQuantity;
    }
}
