package main.tp10.Ejercicio1;

import main.tp10.LookForFiles;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        LookForFiles lookForFiles = new LookForFiles();

        System.out.println("Enter the path of the text file to analyze as the following example: ");
        System.out.println();
        System.out.println("C:\\Users\\admin\\Desktop\\Test.txt");
        System.out.println();

        String filePath = lookForFiles.lookForFile();

        System.out.println("Press 'C' to print the number of characters in the text, or 'L' to print how many lines it has");
        System.out.println();

        label:
        while (true) {
            String input = scanner.next();
            switch (input) {
                case "C": {
                    Program program = new Program(filePath);
                    int characters = program.countCharacters();
                    System.out.println();
                    System.out.println("The text contains " + characters + " characters");
                    System.out.println();
                    break label;
                }
                case "L": {
                    Program program = new Program(filePath);
                    int lines = program.countLines();
                    System.out.println();
                    System.out.println("The text contains " + lines + " lines");
                    System.out.println();
                    break label;
                }
                default:
                    System.out.println();
                    System.out.println("Please, enter a valid option");
                    System.out.println();
                    break;
            }
        }
    }
}
