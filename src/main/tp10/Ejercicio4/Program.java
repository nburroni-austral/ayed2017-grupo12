package main.tp10.Ejercicio4;

import java.io.*;

public class Program {

    private String filePath, newFileName1, newFileName2;

    public Program(String filePath, String newFileName1, String newFileName2) {
        this.filePath = filePath;
        this.newFileName1 = newFileName1;
        this.newFileName2 = newFileName2;
    }

    public void analyze() {
        try {
            BufferedWriter bw1 = new BufferedWriter(new FileWriter(newFileName1));
            BufferedWriter bw2 = new BufferedWriter(new FileWriter(newFileName2));
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String sCurrentLine = br.readLine();
            while (sCurrentLine != null) {
                int counter = 0;
                char[] country = new char[30];
                int countryPosition = 0;
                char[] population = new char[10];
                int populationPosition = 0;
                while (sCurrentLine.charAt(counter) == ' ' && counter < sCurrentLine.length()) {
                    counter++;
                }
                while (sCurrentLine.charAt(counter) != ' ' && counter < sCurrentLine.length()) {
                    country[countryPosition] = sCurrentLine.charAt(counter);
                    countryPosition++;
                    counter++;
                }
                while (sCurrentLine.charAt(counter) == ' ' && counter < sCurrentLine.length()) {
                    counter++;
                }
                while (sCurrentLine.charAt(counter) != ' ' && counter < sCurrentLine.length()) {
                    population[populationPosition] = sCurrentLine.charAt(counter);
                    populationPosition++;
                    counter++;
                }
                String countryName = String.valueOf(country).trim();
                int populationNumber = Integer.parseInt(String.valueOf(population).trim());
                if (populationNumber < 30000000) {
                    bw1.write(countryName);
                    bw1.newLine();
                } else {
                    bw2.write(countryName);
                    bw2.newLine();
                }
                sCurrentLine = br.readLine();
            }
            bw1.close();
            bw2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
