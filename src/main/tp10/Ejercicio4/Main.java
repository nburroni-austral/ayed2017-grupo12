package main.tp10.Ejercicio4;

import main.tp10.LookForFiles;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        LookForFiles lookForFiles = new LookForFiles();

        System.out.println("Enter the path of the text file to analyze the following example: ");
        System.out.println();
        System.out.println("C:\\Users\\admin\\Desktop\\Test.txt");
        System.out.println();

        String filePath = lookForFiles.lookForFile();

        System.out.println("Enter the directory where the file with the countries with population smaller than 30 million" +
                           " is going to be saved, following the format in the next example: ");
        System.out.println();
        System.out.println("C:\\Users\\admin\\Desktop");
        System.out.println();

        String directoryPath1 = lookForFiles.lookForDirectory();

        System.out.println("Enter the name of this new file: ");
        System.out.println();
        String newFileName1 = directoryPath1 + "\\" + scanner.next() + ".txt";
        System.out.println();

        System.out.println("Enter the directory where the file with countries with population greater or equal to 30 million" +
                           " is going to be saved, following the previous format as well");
        System.out.println();

        String directoryPath2 = lookForFiles.lookForDirectory();

        System.out.println("Enter the name of this new file: ");
        System.out.println();
        String newFileName2 = directoryPath2 + "\\" + scanner.next() + ".txt";
        System.out.println();

        System.out.println("Press '1' to run the analysis");
        System.out.println();

        while (true) {
            String theInput = scanner.next();
            if (theInput.equals("1")) {
                Program program = new Program(filePath, newFileName1, newFileName2);
                program.analyze();
                System.out.println();
                System.out.println("Operation successful");
                System.out.println();
                break;
            } else {
                System.out.println();
                System.out.println("Please, enter a valid option");
                System.out.println();
            }
        }
    }
}
