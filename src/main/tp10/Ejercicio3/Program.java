package main.tp10.Ejercicio3;

import java.io.*;

public class Program {

    private String fileName, newFileName;

    public Program(String fileName, String newFileName) {
        this.fileName = fileName;
        this.newFileName = newFileName;
    }

    public void toUpperCase() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(newFileName))) {
            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                String sCurrentLine = br.readLine();
                while (sCurrentLine != null) {
                    String inUpperCase = sCurrentLine.toUpperCase();
                    bw.write(inUpperCase);
                    bw.newLine();
                    sCurrentLine = br.readLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void toLowerCase() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(newFileName))) {
            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                String sCurrentLine = br.readLine();
                while (sCurrentLine != null) {
                    String inLowerCase = sCurrentLine.toLowerCase();
                    bw.write(inLowerCase);
                    bw.newLine();
                    sCurrentLine = br.readLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
