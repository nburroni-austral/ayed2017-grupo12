package main.tp10.Ejercicio3;

import main.tp10.LookForFiles;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        LookForFiles lookForFiles = new LookForFiles();

        System.out.println("Enter the path of the text file to edit as the following example: ");
        System.out.println();
        System.out.println("C:\\Users\\admin\\Desktop\\Test.txt");
        System.out.println();

        String filePath = lookForFiles.lookForFile();

        System.out.println("Now, please enter the directory where you wish to save the new modified text, like in the next example: ");
        System.out.println();
        System.out.println("C:\\Users\\admin\\Desktop");
        System.out.println();

        String directoryPath = lookForFiles.lookForDirectory();

        System.out.println("Enter the name of the new file: ");
        System.out.println();
        String newFileName = directoryPath + "\\" + scanner.next() + ".txt";
        System.out.println();

        System.out.println("Press '1' to convert the text to Upper Case, or '2' to convert it to Lower Case");
        System.out.println();

        label:
        while (true) {
            String input = scanner.next();
            switch (input) {
                case "1": {
                    Program program = new Program(filePath, newFileName);
                    program.toUpperCase();
                    System.out.println();
                    System.out.println("Operation successful");
                    System.out.println();
                    break label;
                }
                case "2": {
                    Program program = new Program(filePath, newFileName);
                    program.toLowerCase();
                    System.out.println();
                    System.out.println("Operation successful");
                    System.out.println();
                    break label;
                }
                default:
                    System.out.println();
                    System.out.println("Please, enter a valid option");
                    System.out.println();
                    break;
            }
        }
    }
}
