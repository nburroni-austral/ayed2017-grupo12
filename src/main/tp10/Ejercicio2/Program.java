package main.tp10.Ejercicio2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Program {

    private String fileName;

    public Program(String fileName) {
        this.fileName = fileName;
    }

    public int getOccurrences(char character) {
        int occurrences = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String sCurrentLine = br.readLine();
            while (sCurrentLine != null) {
                for (int i = 0; i < sCurrentLine.length(); i++) {
                    if (sCurrentLine.charAt(i) == character) {
                        occurrences++;
                    }
                }
                sCurrentLine = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return occurrences;
    }
}
