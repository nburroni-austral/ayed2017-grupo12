package main.tp10.Ejercicio2;

import main.tp10.LookForFiles;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        LookForFiles lookForFiles = new LookForFiles();

        System.out.println("Enter the path of the text file to analyze as the following example: ");
        System.out.println();
        System.out.println("C:\\Users\\admin\\Desktop\\Test.txt");
        System.out.println();

        String filePath = lookForFiles.lookForFile();

        System.out.println("Enter the character you wish to evaluate: ");
        System.out.println();

        while (true) {
            String theInput = scanner.next();
            char input = theInput.charAt(0);
            if (theInput.length() == 1) {
                Program program = new Program(filePath);
                int occurrences = program.getOccurrences(input);
                System.out.println();
                System.out.println("The character " + input + " has " + occurrences + " occurrences in the text");
                System.out.println();
                break;
            } else {
                System.out.println();
                System.out.println("Please enter a valid character");
                System.out.println();
            }
        }
    }
}
