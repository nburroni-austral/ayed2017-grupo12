package main.tp10.Ejercicio5;

import main.tp10.LookForFiles;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        LookForFiles lookForFiles = new LookForFiles();

        System.out.println("Enter the path of the text file to analyze the following example: ");
        System.out.println();
        System.out.println("C:\\Users\\admin\\Desktop\\Test.txt");
        System.out.println();

        String filePath = lookForFiles.lookForFile();

        System.out.println("Insert the quantity of population to be taken into account: ");
        System.out.println();
        int population = scanner.nextInt();
        System.out.println();

        System.out.println("Enter the directory where the file with the countries with population smaller than " + population +
                           " is going to be saved, following the format in the next example: ");
        System.out.println();
        System.out.println("C:\\Users\\admin\\Desktop");
        System.out.println();

        String directoryPath1 = lookForFiles.lookForDirectory();

        System.out.println("Enter the name of this new file: ");
        System.out.println();
        String newFileName1 = directoryPath1 + "\\" + scanner.next() + ".txt";
        System.out.println();

        System.out.println("Enter the directory where the file with countries with population greater or equal to " + population +
                           " is going to be saved, following the previous format as well");
        System.out.println();

        String directoryPath2 = lookForFiles.lookForDirectory();

        System.out.println("Enter the name of this new file: ");
        System.out.println();
        String newFileName2 = directoryPath2 + "\\" + scanner.next() + ".txt";
        System.out.println();

        System.out.println("Press 'PBI' to save the file with the name of the country and the PBI, or 'POB' to save it with the" +
                           " country name as well as the quantity of population");
        System.out.println("To skip, press 0");
        System.out.println();

        while (true) {
            String input = scanner.next();
            if (input.equals("PBI") || input.equals("POB") || input.equals("0")) {
                Program program = new Program(filePath, newFileName1, newFileName2);
                program.analyze(population, input);
                System.out.println();
                System.out.println("Operation successful");
                System.out.println();
                break;
            } else {
                System.out.println();
                System.out.println("Please, enter a valid option");
                System.out.println();
            }
        }
    }
}
