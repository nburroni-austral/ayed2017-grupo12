package main.tp10.Ejercicio5;

import java.io.*;

public class Program {

    private String filePath, newFileName1, newFileName2;

    public Program(String filePath, String newFileName1, String newFileName2) {
        this.filePath = filePath;
        this.newFileName1 = newFileName1;
        this.newFileName2 = newFileName2;
    }

    public void analyze(int thePopulation, String option) {
        try {
            BufferedWriter bw1 = new BufferedWriter(new FileWriter(newFileName1));
            BufferedWriter bw2 = new BufferedWriter(new FileWriter(newFileName2));
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String sCurrentLine = br.readLine();
            while (sCurrentLine != null) {
                int counter = 0;
                char[] country = new char[30];
                int countryPosition = 0;
                char[] population = new char[10];
                int populationPosition = 0;
                char[] pbi = new char[20];
                int pbiPosition = 0;
                while (sCurrentLine.charAt(counter) == ' ' && counter < sCurrentLine.length()) {
                    counter++;
                }
                while (sCurrentLine.charAt(counter) != ' ' && counter < sCurrentLine.length()) {
                    country[countryPosition] = sCurrentLine.charAt(counter);
                    countryPosition++;
                    counter++;
                }
                while (sCurrentLine.charAt(counter) == ' ' && counter < sCurrentLine.length()) {
                    counter++;
                }
                while (sCurrentLine.charAt(counter) != ' ' && counter < sCurrentLine.length()) {
                    population[populationPosition] = sCurrentLine.charAt(counter);
                    populationPosition++;
                    counter++;
                }
                while (sCurrentLine.charAt(counter) == ' ' && counter < sCurrentLine.length()) {
                    counter++;
                }
                while (counter < sCurrentLine.length()) {
                    if (sCurrentLine.charAt(counter) != ' ') {
                        pbi[pbiPosition] = sCurrentLine.charAt(counter);
                        pbiPosition++;
                        counter++;
                    }
                }
                String countryName = String.valueOf(country).trim();
                String populationData = String.valueOf(population).trim();
                int populationNumber = Integer.parseInt(populationData);
                String pbiNumber = String.valueOf(pbi).trim();
                if (populationNumber < thePopulation) {
                    auxWrite(bw1, option, countryName, populationData, pbiNumber);
                } else {
                    auxWrite(bw2, option, countryName, populationData, pbiNumber);
                }
                sCurrentLine = br.readLine();
            }
            bw1.close();
            bw2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void auxWrite(BufferedWriter bw, String option, String countryName, String populationData, String pbiNumber) throws IOException {
        if (option.equals("PBI")) {
            bw.write(countryName);
            bw.write(" ");
            bw.write(pbiNumber);
            bw.newLine();
        } else if (option.equals("POB")) {
            bw.write(countryName);
            bw.write(" ");
            bw.write(populationData);
            bw.newLine();
        } else {
            bw.write(countryName);
            bw.write(" ");
            bw.write(populationData);
            bw.write(" ");
            bw.write(pbiNumber);
            bw.newLine();
        }
    }
}
