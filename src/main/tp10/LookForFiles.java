package main.tp10;

import java.io.File;
import java.util.Scanner;

public class LookForFiles {

    private Scanner scanner = new Scanner(System.in);

    public String lookForFile() {
        String filePath;
        while (true) {
            filePath = scanner.next();
            System.out.println();
            File file = new File(filePath);
            if (file.exists() && !file.isDirectory()) {
                return filePath;
            } else {
                System.out.println("The file could not be found. Please, check if the path is correct and try again");
                System.out.println();
            }
        }
    }

    public String lookForDirectory() {
        String directoryPath;
        while (true) {
            directoryPath = scanner.next();
            System.out.println();
            File file = new File(directoryPath);
            if (file.exists() && file.isDirectory()) {
                return directoryPath;
            } else {
                System.out.println("The directory does not exist. Please, try again");
                System.out.println();
            }
        }
    }
}
