package main.tp11;

import java.io.*;
import java.util.ArrayList;

public class Blockbuster {
    private RandomAccessFile raf;
    // size: (15+2)x2 Strings, 4 integers, 1 boolean
    private final int MOVIE_BYTE_SIZE = 39;
    private BinarySearchTree<String,Long> index;
    private SerializationUtil s = new SerializationUtil();

    Blockbuster(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        this.raf = new RandomAccessFile(file,"rw");
        index = new BinarySearchTree<>(String::compareTo);
    }

    //read an write movies
    public void writeMovie(Movie movie) throws IOException {
        index.put(movie.getName(),raf.getFilePointer());
        raf.writeUTF(movie.getName());
        raf.writeInt(movie.getMinutes());
        raf.writeUTF(movie.getCategory());
        raf.writeBoolean(movie.isReleased());
    }

    public Movie readMovie() throws IOException {
        return new Movie(raf.readUTF(),raf.readInt(),raf.readUTF(),raf.readBoolean());
    }

    //ABM
    private Movie search(String name) throws IOException {
        beginning();
        Movie movie;
        long filePointer = raf.getFilePointer();
        long length = raf.length() - 8;
        while (filePointer<length){
            movie = readMovie();
            if (movie.getName().equals(name))
                return movie;
            filePointer = raf.getFilePointer();
        }
        return null;
    }
    public String modifyName(String name, String newName) throws IOException {
        beginning();
        Movie movie = search(name);
        if (movie != null){
            movie.setName(newName);
            raf.seek(raf.getFilePointer()-MOVIE_BYTE_SIZE);
            writeMovie(movie);
            return "modified successfully";
        }
        return "movie not found";
    }

    public String modifyMinutes(String name, int newMinutes) throws IOException {
        beginning();
        Movie movie = search(name);
        if (movie != null){
            movie.setMinutes(newMinutes);
            raf.seek(raf.getFilePointer()-MOVIE_BYTE_SIZE);
            writeMovie(movie);
            return "modified successfully";
        }
        return "movie not found";
    }

    public String modifyCategory(String name, String newCategory) throws IOException {
        beginning();
        Movie movie = search(name);
        if (movie != null){
            movie.setCategory(newCategory);
            raf.seek(raf.getFilePointer()-MOVIE_BYTE_SIZE);
            writeMovie(movie);
            return "modified successfully";
        }
        return "movie not found";
    }

    public String modifyReleased(String name, boolean newReleased) throws IOException {
        beginning();
        Movie movie = search(name);
        if (movie != null){
            movie.setReleased(newReleased);
            raf.seek(raf.getFilePointer()-MOVIE_BYTE_SIZE);
            writeMovie(movie);
            return "modified successfully";
        }
        return "movie not found";
    }

    public boolean delete(String name) throws IOException {
        beginning();
        Movie movie = search(name);
        if (movie != null){
            movie.setName("deleted");
            raf.seek(raf.getFilePointer()-MOVIE_BYTE_SIZE);
            writeMovie(movie);
            return true;
        }
        return false;
    }


    //movie queries
    public long moviesAmount() throws IOException {
        return (raf.length()/MOVIE_BYTE_SIZE);
    }

    public String movieDetails(String name) throws IOException {
        Movie movie = search(name);
        if (movie != null) return movie.toString();
        else return "movie not found";
    }

    //movie queries --> filter movies
    public ArrayList<Movie> filterByMinutes(int minutes) throws IOException {
        beginning();
        Movie movie;
        ArrayList<Movie> result = new ArrayList<>();
        while (raf.getFilePointer() < raf.length()){
            movie = readMovie();
            if (movie != null && movie.getMinutes() == minutes) result.add(movie);
        }
        return result;
    }

    public ArrayList<Movie> filterByName(String name) throws IOException {
        beginning();
        Movie movie;
        ArrayList<Movie> result = new ArrayList<>();
        while (raf.getFilePointer() < raf.length()){
            movie = readMovie();
            if (movie != null && movie.getName().equals(name)) result.add(movie);
        }
        return result;
    }

    public ArrayList<Movie> filterByCategory(String category) throws IOException {
        beginning();
        Movie movie;
        ArrayList<Movie> result = new ArrayList<>();
        while (raf.getFilePointer() < raf.length()){
            movie = readMovie();
            if (movie != null && movie.getCategory().equals(category)) result.add(movie);
        }
        return result;
    }

    public ArrayList<Movie> filterByReleased(boolean released) throws IOException {
        beginning();
        Movie movie;
        ArrayList<Movie> result = new ArrayList<>();
        while (raf.getFilePointer() < raf.length()){
            movie = readMovie();
            if (movie != null && movie.isReleased() == released) result.add(movie);
        }
        return result;
    }

    public void beginning() throws IOException {
        raf.seek(0);
    }

    public long actual() throws IOException {
        return raf.getFilePointer();
    }

    public long length() throws IOException {
        return raf.length();
    }

    public void close() throws IOException {
        raf.close();
    }

    public void generateIndexFile() {
        s.writeObject(index,"Index File");
    }
}