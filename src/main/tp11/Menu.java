package main.tp11;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class Menu {
    private ScannerUtil s = new ScannerUtil();
    private Blockbuster blockbuster = new Blockbuster("Blockbuster");

    Menu() throws FileNotFoundException {
    }

    public void mainMenu() throws IOException {
        // Display menu graphics
        System.out.println("============================");
        System.out.println("|      MOVIES DATABASE     |");
        System.out.println("============================");
        System.out.println("| Options:                 |");
        System.out.println("|        1. Add a Movie    |");
        System.out.println("|        2. Delete a movie |");
        System.out.println("|        3. Modify a movie |");
        System.out.println("|        4. Movie queries  |");
        System.out.println("|        5. Movie reports  |");
        System.out.println("|        6. Exit           |");
        System.out.println("============================");
        System.out.println(" Select option: ");
        int selection = s.getInt(1,6);

        // Switch construct
        switch (selection) {
            case 1:
                addMenu();
                break;
            case 2:
                deleteMenu();
                break;
            case 3:
                modifyMenu();
                break;
            case 4:
                queriesMenu();
                break;
            case 5:
                reportsMenu();
                break;
            default:
                blockbuster.close();
                System.exit(0);
        }
    }

    private void queriesMenu() throws IOException {
        System.out.println("=============================");
        System.out.println("|       QUERIES MENU        |");
        System.out.println("=============================");
        System.out.println("| Options:                  |");
        System.out.println("|        1. Movie data      |");
        System.out.println("|        2. Amount of movies|");
        System.out.println("|        3. Filter movies   |");
        System.out.println("|        4. generate index  |");
        System.out.println("|        5. Go back         |");
        System.out.println("=============================");
        System.out.println(" Select option: ");
        int selection = s.getInt(1,5);

        switch (selection) {
            case 1:
                String name = s.requestString("how it's called?");
                System.out.println(blockbuster.movieDetails(name));
                break;
            case 2:
                System.out.println("there are " + blockbuster.moviesAmount() + " in our records");
                break;
            case 3:
                filteringSubMenu();
                break;
            case 4:
                blockbuster.generateIndexFile();
                System.out.println("Index File generated successfully");
                break;
            default:
                mainMenu();
                break;
        }

        queriesMenu();
    }

    private void filteringSubMenu() throws IOException {
        System.out.println("===============================");
        System.out.println("|        FILTER MENU          |");
        System.out.println("===============================");
        System.out.println("| Options:                    |");
        System.out.println("|        1. Filter by name    |");
        System.out.println("|        2. Filter by minutes |");
        System.out.println("|        3. Filter by category|");
        System.out.println("|        4. Filter by released|");
        System.out.println("|        5. Go back           |");
        System.out.println("===============================");
        System.out.println(" Select option: ");
        int selection = s.getInt(1,5);
        int result = 0;
        switch (selection) {
            case 1:
                String name = s.requestString("what's the name?");
                result = blockbuster.filterByName(name).size();
                break;
            case 2:
                int minutes = s.requestInt("how much time?");
                result = blockbuster.filterByMinutes(minutes).size();
                break;
            case 3:
                String category = s.requestString("what's the category?");
                result = blockbuster.filterByCategory(category).size();
                break;
            case 4:
                boolean released = s.requestBoolean("is it released to the public?");
                result = blockbuster.filterByReleased(released).size();
                break;
            case 5:
                queriesMenu();
                break;
        }
        System.out.println("there are " + result + " movies that meet the given condition\n");
        filteringSubMenu();
    }

    private void reportsMenu() throws IOException {
        System.out.println("=============================");
        System.out.println("|       REPORTS MENU        |");
        System.out.println("=============================");
        System.out.println("| Options:                  |");
        System.out.println("|        1. List all movies |");
        System.out.println("|        2. List all movies |");
        System.out.println("|           with certain    |");
        System.out.println("|           condition       |");
        System.out.println("|        3. Go Back         |");
        System.out.println("=============================");
        System.out.println(" Select option: ");
        int selection = s.getInt(1,3);

        switch (selection) {
            case 1:
                blockbuster.beginning();
                while (blockbuster.actual() < blockbuster.length()) {
                    System.out.println(blockbuster.readMovie().toString());
                }
                break;
            case 2:
                ArrayList<Movie> result = new ArrayList<>();
                System.out.println("what would you like to filer? (name / minutes / category / released)\n");
                String element = s.getString();
                switch (element) {
                    case "name":
                        String name = s.requestString("what's the name?");
                        result = blockbuster.filterByName(name);
                        break;
                    case "minutes":
                        int minutes = s.requestInt("how much time?");
                        result = blockbuster.filterByMinutes(minutes);
                        break;
                    case "category":
                        String category = s.requestString("what's the category?");
                        result = blockbuster.filterByCategory(category);
                        break;
                    case "released":
                        boolean released = s.requestBoolean("is it released to the public?");
                        result = blockbuster.filterByReleased(released);
                        break;
                    default:
                        reportsMenu();
                        break;
                }
                System.out.println("the movies that meet the given condition are: \n");
                for (Movie m: result){
                    System.out.println("    " + m.getName());
                }
            default:
                mainMenu();
                return;
        }
        reportsMenu();

    }

    private void modifyMenu() throws IOException {
        String name = s.requestString("how it's called?");
        System.out.println("what would you like to modify? (name / minutes / category / released)\n");
        String element = s.getString();
        switch (element){
            case "name":
                String newName = s.requestString("what's the new name?");
                System.out.println(blockbuster.modifyName(name, newName));
                break;
            case "minutes":
                int newMinutes = s.requestInt("how much time?");
                System.out.println(blockbuster.modifyMinutes(name, newMinutes));
                break;
            case "category":
                String newCategory = s.requestString("what's the new category?");
                System.out.println(blockbuster.modifyCategory(name,newCategory));
                break;
            case "released":
                boolean newReleased = s.requestBoolean("is it released to the public?");
                System.out.println(blockbuster.modifyReleased(name, newReleased));
                break;
        }
        mainMenu();
    }

    private void deleteMenu() throws IOException {
        String name = s.requestString("how it's called?");
        if (blockbuster.delete(name)) System.out.println("it's been deleted successfully \n");
        else System.out.println("movie not found \n");
        mainMenu();
    }

    private void addMenu() throws IOException {
        System.out.println("how many movies would you like to add?\n");
        int amount = s.getInt();
        for (int i = 0; i < amount; i++){
            System.out.println("Movie " + (i+1) + ":\n");
            String name = s.requestString("how it's called?");
            int minutes = s.requestInt("how many minutes it last?");
            String category = s.requestString("which category it falls into? (documentary / action / drama / sci-fi)");
            boolean released = s.requestBoolean("has it been released already?");
            blockbuster.writeMovie(new Movie(name,minutes,category,released));
            System.out.println("movie added successfully\n");
        }
        mainMenu();
    }
}