package main.tp11;

import java.io.Serializable;

public class Movie implements Serializable {
    private String name, category;
    private int minutes;
    private boolean released;

    Movie(String name, int minutes, String category, boolean released) {
        super();
        this.name = name;
        this.minutes = minutes;
        this.category = category;
        this.released = released;
    }

    public String getName() {
        return name;
    }

    public int getMinutes() {
        return minutes;
    }

    public String getCategory() {
        return category;
    }

    public boolean isReleased() {
        return released;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setReleased(boolean released) {
        this.released = released;
    }

    @Override
    public String toString() {
        return "Movie name: " + name + "\n" +
                "Minutes: " + minutes + "\n" +
                "Category: " + category + "\n" +
                "Released to the public: " + released + "\n";
    }
}