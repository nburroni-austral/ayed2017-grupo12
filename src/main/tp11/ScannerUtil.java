package main.tp11;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * ScannerUtil class created to ask for input from the user, and always be sure the input is correct
 */
public class ScannerUtil {
    private Scanner s;

    public ScannerUtil(){
        s = new Scanner(System.in);
    }

    /**
     * requests the user for a String, asks again if the input it's not valid
     * @return a valid String inputted by the user
     */
    public String requestString(String message){
        System.out.println(message + "\n");
        return getNormalizedString();
    }

    /**
     * requests the user for an int, asks again if the input it's not valid
     * @return a valid int inputted by the user
     */
    public int requestInt(String message){
        System.out.println(message + "\n");
        return getInt();
    }

    /**
     * requests the user for a boolean, asks again if the input it's not valid
     * @return a valid boolean inputted by the user
     */
    public boolean requestBoolean(String message){
        System.out.println(message + "\n");
        return getBoolean();
    }

    /**
     * asks for an int, asks again if the input it's not valid
     * @return a valid int input
     */
    public int getInt(){
        try {
            int result = s.nextInt();
            s.nextLine();
            return result;
        }
        catch (InputMismatchException exc){
            System.out.println("please enter an integer");
            s.nextLine();
            return getInt();
        }
    }

    /**
     * asks for an int within the min and max, asks again if the input it's not valid
     * @return a valid int input
     */
    public int getInt(int min, int max){
        try {
            int result = s.nextInt();
            if (result<min){
                System.out.println("must be higher than " + min);
                return getInt(min,max);
            }
            if (result>max){
                System.out.println("must be less than " + max);
                return getInt(min,max);
            }
            s.nextLine();
            return result;
        }
        catch (InputMismatchException exc){
            System.out.println("please enter an integer");
            s.nextLine();
            return getInt();
        }
    }

    /**
     * asks for a String, asks again if the input it's not valid
     * @return a valid String input
     */
    public String getString(){
        String result = s.next();
        if(result.isEmpty()){
            System.out.println("please enter text");
            return getString();
        }
        return result;
    }

    private String getNormalizedString(){
        String result = s.nextLine();
        if(result.isEmpty()){
            System.out.println("please enter text");
            return getNormalizedString();
        }
        return adapt(result.toLowerCase(),15);
    }

    /**
     * asks for a String with a minimum and maximum length, asks again if the input it's not valid
     * @return a valid String input
     */
    public String getString(int minCharacters, int maxCharacters){
        String result = s.next();
        if(result.isEmpty()){
            System.out.println("please enter text");
            return getString();
        }
        else if (result.length()>maxCharacters){
            System.out.println("max characters: " + maxCharacters + " please try again");
            return getString(minCharacters,maxCharacters);
        }
        else if (result.length()< minCharacters){
            System.out.println("min characters: " + minCharacters + " please try again");
            return getString(minCharacters,maxCharacters);
        }
        return result;
    }

    private boolean getBoolean() {
        String result = s.next().toLowerCase();
        switch (result) {
            case "yes":
            case "si":
            case "true":
                return true;
            case "no":
            case "false":
                return false;
            default:
                System.out.println("please enter a boolean");
                return getBoolean();
        }
    }

    public String adapt(String s, int limit){
        StringBuilder result = new StringBuilder(s);
        if (s.length() > limit)
            result = new StringBuilder(s.substring(0, limit));
        else {
            for (int i = s.length(); i < limit; i++)
                result.append(" ");
        }
        return result.toString();
    }
}
