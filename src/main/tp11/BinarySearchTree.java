package main.tp11;

import java.io.Serializable;
import java.util.*;

public class BinarySearchTree<K extends Serializable,V extends Serializable> implements Serializable{
    private Node<K,V> head;
    private Comparator<K> comparator;
    private int size;

    BinarySearchTree(Comparator<K> comparator) {
        this.size = 0;
    }

    public int size() {
        return size;
    }

    public boolean containsKey(K key) {
        return find(head,key) != null;
    }

    private Node<K,V> find(Node<K,V> node, K key) {
        if (node == null) return null;
        int cmp = comparator.compare(key, node.key);
        if (cmp == 0) return node;
        else if (cmp < 0) return find(node.left, key);
        else return find(node.right, key);
    }

    public V get(K key) {
        final Node<K, V> result = find(head, key);
        if (result == null) return null;
        return result.value;
    }

    public void put(K key, V value) {
        head = put(head,key,value);
    }


    private Node<K,V> put(Node<K, V> node, K key, V value) {
        if (node == null){
            size++;
            return new Node<>(key,value);
        }else{
            int cmp = comparator.compare(key,node.key);
            if (cmp < 0) node.left = put(node.left,key,value);
            else if (cmp > 0) node.right= put(node.right,key,value);
            else node.value = value;
            return node;
        }
    }

    public void clear() {
        head = null;
        size = 0;
    }

    private void inOrder(Node<K, V> node, ArrayList<K> keys){
        if (node == null) return;
        inOrder(node.left, keys);
        keys.add(node.key);
        inOrder(node.right, keys);
    }

    private class Node<KEY, VALUE> implements Serializable {
        KEY key;
        VALUE value;
        Node<KEY, VALUE> right;
        Node<KEY, VALUE> left;

        Node(KEY key, VALUE value) {
            this.key = key;
            this.value = value;
            this.left = null;
            this.right = null;
        }
    }
}
