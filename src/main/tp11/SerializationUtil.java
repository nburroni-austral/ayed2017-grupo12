package main.tp11;

import java.io.*;

/**
 * SerializationUtil class has methods to write and read objects in the hard drive
 */
public class SerializationUtil {

    public SerializationUtil(){}

    /**
     * Takes an object and creates a *.ser file containing it
     * @param obj the object to save
     * @param filePath the name of the file containing the object
     */
    public void writeObject(Object obj, String filePath){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            fos = new FileOutputStream(filePath + ".ser");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(fos != null) {
                try {
                    fos.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
            if(oos != null) {
                try {
                    oos.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * reads and returns an Object from a *.ser file
     * @param filePath the file name that contains the object to be read
     * @return the object contained in the file
     */
    public Object readObject(String filePath){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        Object obj = null;
        try{
            fis = new FileInputStream(filePath + ".ser");
            ois = new ObjectInputStream(fis);
            obj = ois.readObject();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        } finally {
            if(fis != null) {
                try {
                    fis.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
            if(ois != null) {
                try {
                    ois.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
        return obj;
    }
}
