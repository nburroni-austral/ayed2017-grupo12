package main.tp1.SortAlgorithms;

/**
 * Created by arimi on 13-Mar-17.
 */

/**
 * In the bubble sort, as elements are sorted they gradually "bubble" (or rise) to their proper location in the array.
 * The bubble sort repeatedly compares adjacent elements of an array.
 * This sorting process continues until the last two elements of the array are compared and swapped if out of order
 */
public class BubbleSort<T extends Comparable<T>>  {

    public void bubbleSort(T[] array){

        for (int i = array.length; i>0; i--){
            T max;
            for (int j = 0; j<i-1;j++){
                if(array[j].compareTo(array[j+1])>0){
                    max = array[j];
                    array[j] = array[j+1];
                    array[j+1] = max;
                }
            }
        }
    }
}

