package main.tp1.SortAlgorithms;
/**
 * Created by arimi on 12-Mar-17.
 */

/**
 *  SelectionSort divides the array into two parts - sorted one and unsorted one. At the beginning, sorted part is empty, while unsorted one contains whole array.
 *  At every step, the algorithm finds minimal element in the unsorted part and adds it to the end of the sorted one. When unsorted part becomes empty, algorithm stops.
 *  When algorithm sorts an array, it swaps first element of unsorted part with minimal element and then it is included to the sorted part
 * @param <T>
 */
public class SelectionSort<T extends Comparable<T>> {

    public void selectionSort(T[] array){
        for (int i = 0; i<array.length-1;i++){
            int min = i;
            for (int j = i+1; j<array.length;j++){
                if (array[j].compareTo(array[min])<0) {
                    min = j;
                }
            }
            T temp = array[i];
            array[i] = array[min];
            array[min] = temp;
        }
    }


    public void recursiveSelectionSort(T[] array){
        recursiveSelectionSort2(array,0);
    }

    private void recursiveSelectionSort2(T[] array, int i){

        if (i<array.length-1){
            recursiveSelectionSort3(array,i,i+1,i);
            recursiveSelectionSort2(array,i+1);
        }
    }

    private void recursiveSelectionSort3(T[] array, int i, int j, int min){
        if(j<array.length){
            if (array[j].compareTo(array[min]) < 0) min = j;
            recursiveSelectionSort3(array,i,j+1,min);
        }
        else{
            T temp = array[min];
            array[min] = array[i];
            array[i] = temp;
        }
    }
}
