package main.tp1.SortAlgorithms;

/**
 * Created by arimi on 12-Mar-17.
 */

/**
 * Each iteration, insertion sort removes one element from the input data, finds the location it belongs within the sorted list, and inserts it there.
 * It repeats until no input elements remain.
 * @param <T>
 */
public class InsertionSort<T extends Comparable<T>> {
    
    public void insertionSort(T[] array){

        for (int i = 1; i<array.length;i++){
            T key = array[i];
            int j = i;
            for (; j>0 && key.compareTo(array[j-1])<0; j--){
                    array[j] = array[j-1];
            }
            array[j]= key;
        }
    }
}
