package main.tp1.SearchAlgorithms;

/**
 * Created by arimi on 12-Mar-17.
 */
/**
 * SequentialSearch searches for an int "a" in a int Array trying every element in the collection until "a" is found or until you reach the end of the collection
 */
public class SequentialSearch {
    public int contains(Integer[] intArray, int a){
        for (int i = 0; i<intArray.length; i++){
            if (i==a) return i;
        }
        return -1;
    }

}
