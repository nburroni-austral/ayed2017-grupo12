package main.tp1.SearchAlgorithms;
import java.util.Random;
/**
 * Created by arimi on 12-Mar-17.
 */

public class RandomIntArray {
    /**
     * creates an Integer array of n length with random numbers from 1-100
     * @param n
     * @return
     */
    public static Integer[] randomIntArray(int n){
        Integer[] a = new Integer[n];
        for (int i = 0; i<n; i++){
            a[i] = new Random().nextInt(100);
        }
        return a;
    }
}
