package main.tp1.SearchAlgorithms;

/**
 * Created by arimi on 12-Mar-17.
 */

/**
 * Binary search requires that the collection is already sorted.
 * Binary search checks the element in the middle of the collection. If the search element is smaller or greater than the middle element, then a sub-array is defined which is then searched again.
 * If the searched element is smaller than the middle element, then the sub-array is searched from the start of the array until the middle element.
 * If the searched element is larger than the middle element, then the sub-array is searched from the middle element until the end of the array.
 * the search is over once the searched element is found or the collection is empty.
 */
public class BinarySearch {
    public static int contains(int[] intArray, int a){
        int start = 0;
        int end = intArray.length-1;
        int index = -1;
        while (start <= end){
            int middle = (start + end)/2;
            if (a < intArray[middle]) end = middle - 1;
            else if (a > intArray[middle]) start = middle + 1;
            else{
                index = middle;
                break;
            }
        }
        return index;
    }

}
