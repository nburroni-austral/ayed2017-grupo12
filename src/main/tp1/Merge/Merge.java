package main.tp1.Merge;

/**
 * takes two sorted arrays and merges them into one full sorted array containing the elements of both previous arrays
 * @param <T>
 */
public class Merge<T extends Comparable<T>> {

    public Comparable[] merge(T[] a, T[] b) {
        Comparable[] c = new Comparable[a.length + b.length];
        int indexA = 0;
        int indexB = 0;
        int indexC = 0;

        for (int i = 0; indexA<a.length && indexB<b.length ; i++) {
            if (a[indexA].compareTo(b[indexB])<=0) {
                c[indexC] = a[indexA];
                ++indexC;
                ++indexA;
            } else {
                c[indexC] = b[indexB];
                ++indexC;
                ++indexB;
            }
        }

        if(indexA == a.length){
            for (int i = indexB; i<b.length;i++){
                c[indexC] = b[i];
                indexC++;
            }
        }
        else if (indexB == b.length){
            for (int i = indexA; i<a.length;i++){
                c[indexC] = a[i];
                indexC++;
            }
        }
        return c;
    }
}
