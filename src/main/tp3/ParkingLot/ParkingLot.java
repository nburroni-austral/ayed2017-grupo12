package main.tp3.ParkingLot;

import struct.impl.StaticStack;

import java.util.ArrayList;
import java.util.List;

public class ParkingLot {

    private List<Car> carList;
    private StaticStack<Car> theParkingLot;
    private StaticStack<Car> theSideWalk;
    private int fare;

    public ParkingLot() {
        carList = new ArrayList<>(50);
        theParkingLot = new StaticStack<>(50);
        theSideWalk = new StaticStack<>(49);
        fare = 0;
    }

    public void addCarToParkingLot(Car car) {
        if (carList.size() < 50 && ! (carList.contains(car))) {
            carList.add(car);
            theParkingLot.push(car);
            fare += 5;
        }
    }

    public void removeCar(Car car) {
        if (carList.contains(car)) {
            while (!(theParkingLot.peek() == car)) {
                theSideWalk.push(theParkingLot.peek());
                theParkingLot.pop();
            }
            carList.remove(car);
            theParkingLot.pop();
        }
        while (! theSideWalk.isEmpty()) {
            theParkingLot.push(theSideWalk.peek());
            theSideWalk.pop();
        }
    }

    public List<Car> getCarList() {
        return carList;
    }

    public StaticStack<Car> getTheParkingLot() {
        return theParkingLot;
    }

    public StaticStack<Car> getTheSideWalk() {
        return theSideWalk;
    }

    public int getFare() {
        return fare;
    }
}
