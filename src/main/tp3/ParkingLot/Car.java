package main.tp3.ParkingLot;

public class Car {

    private String brand, model, color, patent;
    private String carNotFoundError;

    public Car() {
        brand = null;
        model = null;
        color = null;
        patent = null;
        carNotFoundError = "The patent does not belong to any car.\n";
    }

    public void setBrand(String aBrand) {
        brand = aBrand;
    }

    public void setModel(String aModel) {
        if (! (brand == null)) {
            model = aModel;
        }
    }

    public void setColor(String aColor) {
        if (! (brand == null) && ! (model == null)) {
            color = aColor;
        }
    }

    public void setPatent(String aPatent) {
        if (! (brand == null) && ! (model == null) && ! (color == null)) {
            patent = aPatent;
        }
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public String getPatent() {
        return patent;
    }

    public String lookForSpecs(String aPatent) {
        if (patent.equals(aPatent)) {
            return brand + model + color;
        }
        return carNotFoundError;
    }

    public void deleteBrand() {
        if (! (brand == null)) {
            brand = null;
            model = null;
            color = null;
            patent = null;
        }
    }

    public void deleteModel() {
        if (! (model == null)) {
            model = null;
            color = null;
            patent = null;
        }
    }

    public void deleteColor() {
        if (! (color == null)) {
            color = null;
            patent = null;
        }
    }

    public void deletePatent() {
        if (! (patent == null)) {
            patent = null;
        }
    }
}
