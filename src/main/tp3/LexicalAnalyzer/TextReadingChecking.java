package main.tp3.LexicalAnalyzer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class is only to verify that your desired text to check later on with the LexicalAnalyzer is read correctly
 */
public class TextReadingChecking {

    private static final String FILENAME = "C:\\Users\\admin\\IdeaProjects\\ayed2017-grupo12\\LexicalAnalyzerText.txt"; // input your text file location here

    /**
     * This main method returns the plain text, so you can verify it is the one you want to later check with the LexicalAnalyzer
     */
    public static void main(String[] args) {

        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
