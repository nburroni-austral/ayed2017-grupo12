package main.tp3.LexicalAnalyzer;

import struct.impl.DynamicStack;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class is the LexicalAnalyzer
 */
public class LexicalAnalyzer {

    private static final String FILENAME = "C:\\Users\\admin\\IdeaProjects\\ayed2017-grupo12\\LexicalAnalyzerText.txt"; // input your text file location here

    /**
     * This main method checks if the braces, square brackets, and parenthesis of a text open and close correctly
     */
    public static void main(String[] args) {

        DynamicStack<String> textLinesStack1 = new DynamicStack<>();
        DynamicStack<String> textLinesStack2 = new DynamicStack<>();
        DynamicStack<String> textLinesStack3 = new DynamicStack<>();
        DynamicStack<String> textLinesStackAux1 = new DynamicStack<>();
        DynamicStack<String> textLinesStackAux2 = new DynamicStack<>();
        DynamicStack<String> textLinesStackAux3 = new DynamicStack<>();

        Boolean verification1 = null;
        Boolean verification2 = null;
        Boolean verification3 = null;

        boolean totalVerification = false;

        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

            String sCurrentLine = br.readLine();

            while (sCurrentLine != null) {
                textLinesStack1.push(sCurrentLine);
                textLinesStack2.push(sCurrentLine);
                textLinesStack3.push(sCurrentLine);
                textLinesStackAux1.push(sCurrentLine);
                textLinesStackAux2.push(sCurrentLine);
                textLinesStackAux3.push(sCurrentLine);
                sCurrentLine = br.readLine();
            }

            while (! textLinesStack1.isEmpty()) {
                if (textLinesStack1.peek().contains("{")) {
                    while (! textLinesStackAux1.isEmpty() && ! textLinesStackAux1.peek().contains("}")) {
                        if (textLinesStackAux1.size() == 1  && ! textLinesStackAux1.peek().contains("}")) {
                            verification1 = false;
                            break;
                        }
                        textLinesStackAux1.pop();
                    }
                    if (textLinesStackAux1.peek().contains("}")) {
                        verification1 = true;
                        textLinesStackAux1.pop();
                    }
                }
                textLinesStack1.pop();
            }

            if (verification1 == null) {
                System.out.println("Braces are never used");
            } else if (verification1) {
                System.out.println("Every brace opened closes correctly");
            } else {
                System.out.println("The braces do not close correctly");
            }

            while (! textLinesStack2.isEmpty()) {
                if (textLinesStack2.peek().contains("[")) {
                    while (! textLinesStackAux2.isEmpty() && ! textLinesStackAux2.peek().contains("]")) {
                        if (textLinesStackAux2.size() == 1  && ! textLinesStackAux2.peek().contains("]")) {
                            verification2 = false;
                            break;
                        }
                        textLinesStackAux2.pop();
                    }
                    if (textLinesStackAux2.peek().contains("]")) {
                        verification2 = true;
                        textLinesStackAux2.pop();
                    }
                }
                textLinesStack2.pop();
            }

            if (verification2 == null) {
                System.out.println("Square brackets are never used");
            } else if (verification2) {
                System.out.println("Every square bracket opened closes correctly");
            } else {
                System.out.println("The square brackets do not close correctly");
            }

            while (! textLinesStack3.isEmpty()) {
                if (textLinesStack3.peek().contains("(")) {
                    while (! textLinesStackAux3.isEmpty() && ! textLinesStackAux3.peek().contains(")")) {
                        if (textLinesStackAux3.size() == 1  && ! textLinesStackAux3.peek().contains(")")) {
                            verification3 = false;
                            break;
                        }
                        textLinesStackAux3.pop();
                    }
                    if (textLinesStackAux3.peek().contains(")")) {
                        verification3 = true;
                        textLinesStackAux3.pop();
                    }
                }
                textLinesStack3.pop();
            }

            if (verification3 == null) {
                System.out.println("Parenthesis are never used");
            } else if (verification3) {
                System.out.println("Every parenthesis opened closes correctly");
            } else {
                System.out.println("The parenthesis do not close correctly");
            }

            if (verification1 != null && verification2 != null && verification3 != null) {
                if (verification1 && verification2 && verification3) {
                    totalVerification = true;
                }
            }

            if (totalVerification) {
                System.out.println("The use of braces, square brackets, and parenthesis is flawless");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
