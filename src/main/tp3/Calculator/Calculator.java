package main.tp3.Calculator;

import struct.impl.DynamicStack;

/**
 * Calculator class creates a calculator capable of solving equations entered by the user
 */
public class Calculator {
    private DynamicStack<Double> numbersStack = new DynamicStack<>();
    private String equation = "";

    /**
     *main method, receives the String entered by the user and returns the result as a Double
     * @param eq equation entered by the user
     * @return the result of the equation
     */
    public Double solveEquation(String eq){
        Double result = 0.0;
        String equation = eq;
        DynamicStack<String> sumOperations = new DynamicStack<>();
        DynamicStack<String> subOperations = new DynamicStack<>();
        boolean finalSum = true;
        while (equation.length()>0){
            if (nextSumIndex(equation) >= 0 && nextSubIndex(equation) >= 0){
                if (nextSumIndex(equation)< nextSubIndex(equation)){
                    String operation = equation.substring(0,nextSumIndex(equation));
                    sumOperations.push(operation);
                    equation = equation.substring(nextSumIndex(equation) + 1);
                    operation = equation.substring(0,Math.min(nextSumIndex(equation),nextSubIndex(equation)));
                    sumOperations.push(operation);
                    finalSum = true;
                }
                else{
                    String operation = equation.substring(0,nextSubIndex(equation));
                    subOperations.push(operation);
                    equation = equation.substring(nextSubIndex(equation) + 1);
                    finalSum = false;
                }
            }
            else if (nextSumIndex(equation) >= 0 && nextSubIndex(equation) < 0){
                String operation = equation.substring(0,nextSumIndex(equation));
                sumOperations.push(operation);
                equation = equation.substring(nextSumIndex(equation) + 1);
                finalSum = true;
            }
            else if (nextSumIndex(equation) < 0 && nextSubIndex(equation) >= 0) {
                String operation = equation.substring(0,nextSubIndex(equation));
                subOperations.push(operation);
                equation = equation.substring(nextSubIndex(equation) + 1);
                finalSum = false;
            }
            else{
                if (finalSum) {
                    sumOperations.push(equation);
                    equation = "";
                }
                else {
                    subOperations.push(equation);
                    equation = "";
                }

            }
        }
        while (sumOperations.size() > 0){
            result += solve(sumOperations.peek());
            sumOperations.pop();
        }
        while (subOperations.size() > 0){
            if (subOperations.size() == 1) result += solve(subOperations.peek());
            else result -= solve(subOperations.peek());
            subOperations.pop();
        }

        return result;
    }

    /**
     * auxiliary method, calculates a single operation in String type and returns the result as Double
     * @param equationString operation to be solved
     * @return result of the operation
     */
    private Double solve(String equationString){
        this.equation = equationString;
        Double result;
        while(nextSymbol(equation) > 0){
            numbersStack.push(getNextNumber());
            if (nextSymbol(equation) == 4){
                deleteSymbol();
                result = numbersStack.peek() / getNextNumber();
                popPush(result);
            }
            else if (nextSymbol(equation) == 3){
                if (nextSymbol(equation.substring(1)) == 4){
                    deleteSymbol();
                    Double up = getNextNumber();
                    deleteSymbol();
                    Double fraction = up / getNextNumber();
                    Double mult = numbersStack.peek() * fraction;
                    popPush(mult);
                }
                else{
                    deleteSymbol();
                    Double mult = numbersStack.peek() * getNextNumber();
                    popPush(mult);
                }
            }
            else if (nextSymbol(equation) == 2) {
                if (nextSymbol(equation.substring(1)) == 4) {
                    deleteSymbol();
                    Double upper = getNextNumber();
                    deleteSymbol();
                    Double fraction = upper / getNextNumber();
                    Double sub = numbersStack.peek() - fraction;
                    popPush(sub);
                }
                if (nextSymbol(equation.substring(1)) == 3) {
                    deleteSymbol();
                    Double next = getNextNumber();
                    if (nextSymbol(equation.substring(1)) == 4) {
                        deleteSymbol();
                        Double upper = getNextNumber();
                        deleteSymbol();
                        Double fraction = upper / getNextNumber();
                        Double mult = next * fraction;
                        Double sub = numbersStack.peek() - mult;
                        popPush(sub);
                    }
                } else {
                    deleteSymbol();
                    Double sub = numbersStack.peek();
                    sub -= getNextNumber();
                    popPush(sub);
                }
            }
            else if (nextSymbol(equation) == 1){
                if (nextSymbol(equation.substring(1)) == 4){
                    deleteSymbol();
                    Double upper = getNextNumber();
                    deleteSymbol();
                    Double fraction = upper / getNextNumber();
                    Double sum = numbersStack.peek() + fraction;
                    popPush(sum);
                }
                if (nextSymbol(equation.substring(1)) == 3){
                    deleteSymbol();
                    Double next = getNextNumber();
                    if (nextSymbol(equation.substring(1)) == 4){
                        deleteSymbol();
                        Double upper = getNextNumber();
                        deleteSymbol();
                        Double fraction = upper / getNextNumber();
                        Double mult = next*fraction;
                        Double sum = numbersStack.peek() + mult;
                        popPush(sum);
                    }
                }
                else{
                    deleteSymbol();
                    Double sum = numbersStack.peek();
                    sum += getNextNumber();
                    popPush(sum);
                }
            }
        }
        if (numbersStack.isEmpty()) return Double.parseDouble(equationString);
        result = numbersStack.peek();
        numbersStack.empty();
        return result;
    }

    //returns the next number of the equation and takes it out of the equation string
    private Double getNextNumber(){
        Double result;
        int indexSum = equation.indexOf('+');
        int indexSub = equation.indexOf('-');
        int indexMul = equation.indexOf('*');
        int indexDiv = equation.indexOf('/');
        if (indexSum < 0) indexSum = 100;
        if (indexSub < 0) indexSub = 100;
        if (indexDiv < 0) indexDiv = 100;
        if (indexMul < 0) indexMul = 100;
        if (indexDiv == 100  && indexMul == 100  && indexSum == 100  && indexSub == 100) {
            return Double.parseDouble(equation);
        }
        int min = Math.min(Math.min(indexSum,indexSub),Math.min(indexMul,indexDiv));
        if (min == 0) return Double.parseDouble(equation.substring(1));
        result = Double.parseDouble(equation.substring(0,min));
        equation = equation.substring(min);
        return result;
    }
    //returns which is the next operator symbol
    private int nextSymbol(String equation){
        for (int i = 0; i < equation.length(); i++){
            if (equation.charAt(i) == '+') return 1;
            else if (equation.charAt(i) == '-') return 2;
            else if (equation.charAt(i) == '*') return 3;
            else if (equation.charAt(i) == '/') return 4;
        }
        return -1;
    }
    //returns the index of the sum symbol, or -1 if there is nonee
    private int nextSumIndex (String eq){
        for (int i = 0; i < eq.length(); i++){
            if (eq.charAt(i) == '+') return i;
        }
        return -1;
    }
    //returns the index of the substract symbol, or -1 if there is none
    private int nextSubIndex (String eq){
        for (int i = 0; i < eq.length(); i++){
            if (eq.charAt(i) == '-') return i;
        }
        return -1;
    }
    //deletes the first operator symbol on the equation string
    private void deleteSymbol(){
        equation = equation.substring(1);
    }
    //pops the stack and push a new value
    private void popPush(Double d){
        numbersStack.pop();
        numbersStack.push(d);
    }
}