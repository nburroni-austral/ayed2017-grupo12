package main.tp6;

/**
 * Main class to test and run the App
 */
public class Main {

    public static void main(String[] args) {

        BusApp busApp = new BusApp();
        busApp.start();
    }
}
