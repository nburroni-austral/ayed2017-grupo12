package main.tp6;

import main.tp11.SerializationUtil;
import main.tp11.ScannerUtil;
import struct.impl.DynamicSortedList;

/**
 * BusSystem class has the list of Buses and methods to manage them
 */
public class BusSystem {

    private DynamicSortedList<Bus> busesList;
    private SerializationUtil ser;

    /**
     * creates a Bus System
     */
    BusSystem(){
        busesList = new DynamicSortedList<>();
        ser = new SerializationUtil();
    }

    /**
     * adds a bus to the system
     * @param lineNumber the line number of the bus
     * @param internNumber the intern number of the bus
     * @param seatsQuantity the amount of seats of the bus
     * @param isDisableAvailable if the bus has available seats for disabled people
     */
    public void addBus(int lineNumber, int internNumber, int  seatsQuantity, boolean isDisableAvailable) {
        Bus bus = new Bus(lineNumber, internNumber, seatsQuantity, isDisableAvailable);
        busesList.insert(bus);
    }

    /**
     * deletes a bus from the system
     * @param lineNumberToDelete the line number of the bus
     * @param internNumberToDelete the intern number of the bus
     * @return true if deleted, false if not found
     */
    public boolean deleteBus(int lineNumberToDelete, int internNumberToDelete) {
        for (int i = 0; i < busesList.size(); i++) {
            busesList.goTo(i);
            if (busesList.getActual().getLineNumber() == lineNumberToDelete && busesList.getActual().getInternNumber() == internNumberToDelete) {
                busesList.remove();
                return true;
            }
        }
        return false;
    }

    /**
     * makes a report of the Buses in the system
     */
    public void makeReport() {
        String isDisabledAvailable;
        for (int i = 0; i < busesList.size(); i++) {
            busesList.goTo(i);
            System.out.println("Bus #" + (i + 1));
            System.out.println("Line Number: " + busesList.getActual().getLineNumber());
            System.out.println("Intern Number: " + busesList.getActual().getInternNumber());
            System.out.println("Quantity of seats: " + busesList.getActual().getSeatsQuantity());
            if (busesList.getActual().isDisableAvailable()) isDisabledAvailable = "YES";
            else isDisabledAvailable = "NO";
            System.out.println("Is suitable for disabled people: " + isDisabledAvailable);
            System.out.println();
        }
        System.out.println();
    }

    /**
     * makes a report of the Buses in the system that have available seats for disabled people
     */
    public void disabledSuitableReport() {
        for (int i = 0; i < busesList.size(); i++) {
            int busesDisabledSuitable = 0;
            busesList.goTo(i);
            int lineNumber = busesList.getActual().getLineNumber();
            if (busesList.getActual().isDisableAvailable()) busesDisabledSuitable++;
            while (busesList.getActualPosition() < busesList.size() - 1) {
                busesList.goNext();
                if (busesList.getActual().getLineNumber() == lineNumber) {
                    i++;
                    if (busesList.getActual().isDisableAvailable()) {
                        busesDisabledSuitable++;
                    }
                }
            }
            System.out.println("In Line Number " + lineNumber + " there are a total of " + busesDisabledSuitable + " buses suitable for disabled people");
        }
        System.out.println();
    }

    /**
     * makes a report of the Buses in the system that have 27 or more seats
     */
    public void plus27SeatsReport() {
        for (int i = 0; i < busesList.size(); i++) {
            int plus27SeatsBuses = 0;
            busesList.goTo(i);
            int lineNumber = busesList.getActual().getLineNumber();
            if (busesList.getActual().getSeatsQuantity() > 27) plus27SeatsBuses++;
            while (busesList.getActualPosition() < busesList.size() - 1) {
                busesList.goNext();
                if (busesList.getActual().getLineNumber() == lineNumber) {
                    i++;
                    if (busesList.getActual().getSeatsQuantity() > 27) {
                        plus27SeatsBuses++;
                    }
                }
            }
            System.out.println("In Line Number " + lineNumber + " there are a total of " + plus27SeatsBuses + " buses with more than 27 seats");
        }
        System.out.println();
    }

    /**
     * saves the list in the folder of the project
     * @param fileName the name of the file to be saved to
     */
    public void saveList(String fileName){
        ser.writeObject(busesList,fileName);
    }

    /**
     * reads a list from the project and uses it as the system list
     * @param fileName the name of the file to be read from
     */
    public void readList(String fileName){
        ser.readObject(fileName);
    }
}
