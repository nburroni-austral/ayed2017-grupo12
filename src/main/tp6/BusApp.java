package main.tp6;

import main.tp11.ScannerUtil;

/**
 * BusApp class creates an application to test the BusSystem
 */
public class BusApp {
    private ScannerUtil input;
    private BusSystem busSystem;

    /**
     * creates a Bus App
     */
    public BusApp(){
        this.input = new ScannerUtil();
        this.busSystem = new BusSystem();
    }

    /**
     * reads the user input and calls the method to be used accordingly
     * @param selected the option the user selected
     */
    private void mainMenu(int selected){
        switch (selected) {
            case 1:
                addBusMenu();
                break;
            case 2:
                deleteBusMenu();
                break;
            case 3:
                busSystem.makeReport();
                break;
            case 4:
                busSystem.disabledSuitableReport();
                break;
            case 5:
                busSystem.plus27SeatsReport();
                break;
            case 6:
                saveList();
                break;
            case 7:
                readList();
                break;
            case 8:
                System.exit(0);
            default:
                System.out.println("Please enter a valid number\n");
                mainMenu(input.getInt());
        }
    }

    /**
     * prints the menu options
     */
    private void printMenu(){
        System.out.println("1. Add buses\n" +
                "2. Delete buses\n" +
                "3. Report by line number and in the same line by intern number\n" +
                "4. Inform how many buses for disabled people are available by bus line\n" +
                "5. Inform how many buses with 27 or more seats are available by bus line\n" +
                "6. Save list\n" +
                "7. Read list\n" +
                "8. Exit\n");
    }

    /**
     * starts the app
     */
    void start(){
        printMenu();
        int selected = input.getInt();
        mainMenu(selected);
        if (selected != 0)
            start();
    }

    /**
     * asks for the new Bus details and add a new Bus to the system
     */
    private void addBusMenu() {
        System.out.println("Enter the Line number: ");
        int lineNumber = input.getInt();
        System.out.println("Enter the Intern number: ");
        int internNumber = input.getInt();
        System.out.println("Enter the quantity of seats: ");
        int seatsQuantity = input.getInt();
        System.out.println("Press '0' if the bus is suitable for disabled people, or '1' if it is not: ");
        int disableAvailable = input.getInt();
        boolean isDisableAvailable = false;
        if (disableAvailable == 0) isDisableAvailable = true;
        busSystem.addBus(lineNumber, internNumber, seatsQuantity, isDisableAvailable);
        System.out.println();
    }

    /**
     * asks for the details of the bus to be deleted from the system
     */
    private void deleteBusMenu() {
        System.out.println("Enter the Line number of the bus to delete: ");
        int lineNumberToDelete = input.getInt();
        System.out.println("Enter the Intern number of the bus to delete: ");
        int internNumberToDelete = input.getInt();
        if (busSystem.deleteBus(lineNumberToDelete,internNumberToDelete))
            System.out.println("The bus was successfully removed\n");
        else System.out.println("The bus could not be found\n");
    }

    /**
     * asks for the details of the file to save the list to
     */
    private void saveList(){
        System.out.println("Enter the name of the file to be saved:");
        String fileName = input.getString();
        busSystem.saveList(fileName);
        System.out.println("File saved successfully\n");
    }

    /**
     * asks for the details of the file to read the list from
     */
    private void readList() {
        System.out.println("Enter the name of the file to be read:");
        String fileName = input.getString();
        busSystem.readList(fileName);
        System.out.println("File read successfully\n");
    }
}
