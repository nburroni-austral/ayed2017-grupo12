package main.tp6;

import java.io.Serializable;

/**
 * Bus class creates a Bus model to be used by the BusSystem
 */
public class Bus implements Comparable<Bus>,Serializable {
    //serialization Key
    private static final long serialVersionUID = -55857686305273843L;

    private int lineNumber, internNumber, seatsQuantity;
    private boolean disableAvailable;

    /**
     * creates a bus with it's attributes
     * @param lineNumber bus line number
     * @param internNumber bus intern number
     * @param seatsQuantity amount of seats the bus has
     * @param disableAvailable if the bus has available seats for disabled people
     */
    public Bus(int lineNumber, int internNumber, int seatsQuantity, boolean disableAvailable) {
        this.lineNumber = lineNumber;
        this.internNumber = internNumber;
        this.seatsQuantity = seatsQuantity;
        this.disableAvailable = disableAvailable;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public int getInternNumber() {
        return internNumber;
    }

    public int getSeatsQuantity() {
        return seatsQuantity;
    }

    public boolean isDisableAvailable() {
        return disableAvailable;
    }

    /**
     * compares Buses by line number or intern number if the line numbers are equal
     * @param other the other bus to compare to
     * @return less than 0 if is less, 0 if they are equal, grater than 0 if is greater
     */
    public int compareTo(Bus other) {
        if (this.getLineNumber() != other.getLineNumber()) {
            return Integer.compare(this.getLineNumber(), other.getLineNumber());
        } else {
            return Integer.compare(this.getInternNumber(), other.getInternNumber());
        }
    }
}
