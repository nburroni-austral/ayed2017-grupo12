package main.tp5.Franco;

import struct.impl.DynamicList;

import java.util.Random;

public class Simulation {

    private DynamicList<Booth> boothList;
    private int boothQuantity, openingTime, closingTime;   // the time is evaluated in seconds

    public Simulation(int boothQuantity) {
        openingTime = 0;      // opens at 6:00
        closingTime = 57600;  // closes at 22:00
        if (boothQuantity >= 3 && boothQuantity <= 10) {
            this.boothQuantity = boothQuantity;
            boothList = new DynamicList<>();
            for (int i = 0; i < boothQuantity; i++) {
                boothList.insertNext(new Booth());
            }
        } else {
            System.out.println("Please, restart the program and enter a valid number of booths, between 3 and 10");
            System.exit(1);
        }
    }

    public void simulate() {

        Random random = new Random();

        for (int i = openingTime; i < closingTime; i++) {
            if (i % 10 == 0) {
                for (int j = 0; j < 5; j++) {
                    int boothChoose = random.nextInt(boothQuantity);
                    boothList.goTo(boothChoose);
                    Person person = new Person(i);
                    boothList.getActual().getPeopleQueue().enqueue(person);
                }
                for (int j = 0; j < boothQuantity; j++) {
                    int check = random.nextInt(100);
                    boothList.goTo(j);
                    if (! boothList.getActual().getPeopleQueue().isEmpty()) {
                        if (check < 30) {
                            boothList.getActual().getPeopleQueue().dequeue();
                            boothList.getActual().setPeopleAttended(boothList.getActual().getPeopleAttended() + 1);
                            boothList.getActual().setMoney(boothList.getActual().getMoney() + 0.70);
                        } else {
                            boothList.getActual().setIdleTime(boothList.getActual().getIdleTime() + 10);
                        }
                    } else {
                        boothList.getActual().setIdleTime(boothList.getActual().getIdleTime() + 10);
                    }
                }
            }
            if (i >= closingTime - 30) {
                for (int j = 0; j < boothQuantity; j++) {
                    boothList.goTo(j);
                    while (! boothList.getActual().getPeopleQueue().isEmpty()) {
                        boothList.getActual().getPeopleQueue().dequeue();
                        boothList.getActual().setPeopleAttended(boothList.getActual().getPeopleAttended() + 1);
                        boothList.getActual().setMoney(boothList.getActual().getMoney() + 0.70);
                    }
                }
            }
        }
    }

    public int getClosingTime() {
        return closingTime;
    }

    public DynamicList<Booth> getBoothList() {
        return boothList;
    }
}
