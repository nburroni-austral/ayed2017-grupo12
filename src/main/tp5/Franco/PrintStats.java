package main.tp5.Franco;

import java.util.Scanner;

public class PrintStats {

    public PrintStats() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the quantity of booths: ");
        int boothQuantity  = scanner.nextInt();

        Simulation simulation = new Simulation(boothQuantity);
        simulation.simulate();

        System.out.println();
        System.out.println("The following represent the stats for a working day");
        System.out.println();

        int totalPeopleAttended = 0;
        for (int i = 0; i < boothQuantity; i++) {
            simulation.getBoothList().goTo(i);
            System.out.println("Booth #" + (i+1));
            double averageWait = simulation.getClosingTime() / simulation.getBoothList().getActual().getPeopleAttended();
            System.out.println("Average waiting time: " + averageWait + " seconds");
            double totalMoney = simulation.getBoothList().getActual().getMoney();
            System.out.println("Total money collected: $" + totalMoney);
            double totalIdleTime = simulation.getBoothList().getActual().getIdleTime();
            System.out.println("Idle time: " + totalIdleTime + " seconds");
            totalPeopleAttended += simulation.getBoothList().getActual().getPeopleAttended();
            System.out.println();
        }
        System.out.println("A total of " + totalPeopleAttended + " people was attended across all booths");
        System.out.println();
    }
}
