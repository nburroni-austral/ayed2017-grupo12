package main.tp5.Franco;

import struct.impl.DynamicQueue;

public class Booth {

    private DynamicQueue<Person> peopleQueue;
    private double money, idleTime, peopleAttended;

    public Booth() {
        peopleQueue = new DynamicQueue<>();
        money = 0;
        idleTime = 0;
        peopleAttended = 0;
    }

    public DynamicQueue<Person> getPeopleQueue() {
        return peopleQueue;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getMoney() {
        return money;
    }

    public void setIdleTime(double idleTime) {
        this.idleTime = idleTime;
    }

    public double getIdleTime() {
        return idleTime;
    }

    public void setPeopleAttended(double peopleAttended) {
        this.peopleAttended = peopleAttended;
    }

    public double getPeopleAttended() {
        return peopleAttended;
    }
}
