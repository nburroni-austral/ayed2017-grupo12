package main.tp5.Ariel;

import struct.impl.DynamicList;

import java.util.Random;
import java.util.Scanner;

public class Simulation {

    private int openTime = 57600;
    private int criticalTime = 57570;
    private int interval = 10;
    private int timePassed;
    private DynamicList<Cashier> cashiers = new DynamicList<>();

    public Simulation(int cashiersAmount) {
        for (int i = 0; i < cashiersAmount; i++){
            cashiers.insertNext(new Cashier(criticalTime));
        }
    }

    public void setup(int openTime, int criticalTime, int interval){
        this.openTime = openTime;
        this.criticalTime = criticalTime;
        this.interval = interval;
    }

    public void run(int clientsPerInterval){
        for (timePassed = 0; timePassed < openTime; timePassed += interval){
            sendClients(clientsPerInterval);
        }
        report();
    }

    private void report() {
        for (int i = 0; i < cashiers.size(); i++){
            cashiers.goTo(i);
            System.out.println("report of cashier number " + (i+1) + ":");
            System.out.println("medium time waited: " + cashiers.getActual().getMediumWaitingTime()*interval);
            System.out.println("total funds obtained: " + cashiers.getActual().getFunds());
            System.out.println("total free time: " + cashiers.getActual().getFreeTime()*interval);
            System.out.println("\n");
        }
    }

    private void sendClients(int clientsAmount) {
        Random r = new Random();
        for (int i = 0; i < clientsAmount; i++){
            cashiers.goTo(r.nextInt(cashiers.size()));
            cashiers.getActual().addClient(new Client(timePassed));
        }
        for (int i = 0; i < cashiers.size(); i++){
            cashiers.goTo(i);
            cashiers.getActual().attend(timePassed);
        }
    }
}
