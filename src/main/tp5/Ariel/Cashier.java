package main.tp5.Ariel;

import struct.impl.DynamicQueue;

import java.util.Random;

public class Cashier {

    final static float ticketCost = 0.7f;
    final static float probability = 0.3f;
    private int clientsAttended;
    private float totalTime;
    private int criticalTime;
    private float freeTime;
    private float funds;
    private DynamicQueue<Client> clients = new DynamicQueue<>();
    Random r = new Random();

    public Cashier(int criticalTime) {
        this.criticalTime = criticalTime;
    }

    public void addClient(Client client){
        clients.enqueue(client);
    }

    public void attend(int time){
        if (clients.isEmpty()){
            freeTime++;
        }
        else if (time >= criticalTime) emptyQueue(time);
        else if (r.nextFloat() <= probability){
            totalTime += clients.dequeue().timeWaited(time);
            clientsAttended++;
            funds += ticketCost;
        }
    }

    public float getMediumWaitingTime(){
        return totalTime/clientsAttended;
    }

    private void emptyQueue(int time){
        if (clients.isEmpty()){
            freeTime++;
        }
        else{
            for (int i = 0; i < clients.size(); i++){
                totalTime += clients.dequeue().timeWaited(time);
                clientsAttended++;
                funds += ticketCost;
            }
        }
    }

    public float getFreeTime() {
        return freeTime;
    }

    public float getFunds() {
        return funds;
    }
}
