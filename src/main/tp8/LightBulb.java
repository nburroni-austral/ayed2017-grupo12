package main.tp8;

/**
 * LightBulb class creates a type of light bulb to be stocked, has all the information the user needs.
 */
public class LightBulb implements Comparable<LightBulb> {
    private int watts, stock;
    private String code,type;

    LightBulb(String code, String type, int watts, int stock) {
        this.code = code;
        this.type = type;
        this.watts = watts;
        this.stock = stock;
    }

    /**
     * updates the stock of a light bulb
     * @param stock the new Stock
     */
    void updateStock(int stock) {
        this.stock = stock;
    }

    /**
     * creates a report of the information of the light bulb
     */
    void report(){
        System.out.println("light bulb code: " + code + "\n" +
                "light bulb type: " + type + "\n" +
                "light bulb watts: " + watts + "\n" +
                "light bulb stock left: " + stock + "\n" + "\n");
    }

    @Override
    public int compareTo(LightBulb lb) {
        return this.code.compareTo(lb.code);
    }
}
