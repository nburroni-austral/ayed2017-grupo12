package main.tp8;
import struct.impl.SearchBinaryTree;
import java.util.LinkedList;

/**
 * SearchBinaryApi has methods to modify or display the SearchBinaryTree of light bulbs (stock)
 */
public class SearchBinaryApi {

    /**
     * displays the information of all light bulbs on stock in pre-order
     * @param sbt the SearchBinaryTree of light bulbs (stock)
     */
    public void preOrderReport(SearchBinaryTree<LightBulb> sbt){
        if (sbt.isEmpty()) return;
        sbt.getRoot().report();
        preOrderReport(sbt.getLeft());
        preOrderReport(sbt.getRight());
    }

    /**
     * merges the stock saved on a LinkedList to the new SearchBinaryTree
     * @param sbt the SearchBinaryTree
     * @param oldList the LinkedList used before
     * @return the new SearchBinaryTree filled with all the information the List had
     */
    public SearchBinaryTree<LightBulb> mergeFromList(SearchBinaryTree<LightBulb> sbt,LinkedList<LightBulb> oldList) {
        for (LightBulb lb: oldList){
            sbt.insert(lb);
            oldList.remove(lb);
        }
        return sbt;
    }
}
