package main.tp8;

/**
 * Exception created to throw when the light bulb it's not in the stock
 */
public class LightBulbNotFoundException extends RuntimeException {
}
