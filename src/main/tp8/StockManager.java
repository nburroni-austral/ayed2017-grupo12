package main.tp8;

import main.tp11.ScannerUtil;
import struct.impl.ElementNotInTreeException;
import struct.impl.SearchBinaryTree;

import java.util.LinkedList;

/**
 * StockManager class creates a program that can manage the stock via input from the user
 */
public class StockManager {
    private SearchBinaryTree<LightBulb> tree;
    private SearchBinaryApi api;
    private ScannerUtil s;

    /**
     * creates an empty stock
     */
    public StockManager(){
        tree = new SearchBinaryTree<>();
        api = new SearchBinaryApi();
        s = new ScannerUtil();

    }

    /**
     * creates a stock from a List
     */
    public StockManager(LinkedList<LightBulb> oldList){
        tree = api.mergeFromList(tree,oldList);
        s = new ScannerUtil();
    }


    private void updateStock(String lCode, int newStock){
        if (tree.isEmpty()) throw new LightBulbNotFoundException();
        try{
            tree.search(new LightBulb(lCode,"",0,0)).updateStock(newStock);
        }
        catch (ElementNotInTreeException e){
            throw new LightBulbNotFoundException();
        }
    }

    private void removeLb(String lCode){
        if (tree.isEmpty() || !tree.exists(new LightBulb(lCode,"",0,0))){
            System.out.println("light bulb not in the stock");
            return;
        }
        tree.remove(new LightBulb(lCode,"",0,0));
    }

    private void printMenu(){
        System.out.println("1. add a new light bulb to the stock\n" +
                "2. update the stock of a light bulb in the stock\n" +
                "3. remove a light bulb from the stock\n" +
                "4. create a report of all current light bulbs on the stock\n" +
                "0. exit");
    }

    void menu(){
        printMenu();
        int selected = s.getInt();
        mainMenu(selected);
        if (selected != 0)
            menu();
    }

    private void mainMenu(int selected){
        String aux;
        switch (selected) {
            case 1:
                generateLb();
                break;
            case 2:
                System.out.println("enter the code of the light bulb you want to update: ");
                aux = s.getString(5,5);
                System.out.println("enter the new Stock: ");
                int newStock = s.getInt();
                updateStock(aux,newStock);
                break;
            case 3:
                System.out.println("enter the code of the light bulb to remove: ");
                aux = s.getString(5,5);
                removeLb(aux);
                break;
            case 4:
                report();
                break;
        }
    }

    private void generateLb(){
            String lCode,lType;
            int lWatts,lStock;
            System.out.println("\nEnter light bulb code: (5 characters)");
            lCode = s.getString(5,5);

            System.out.println("\nEnter the amount of Watts it has: (integer)");
            lWatts = s.getInt(15,600);

            System.out.println("\nEnter the light bulb type: (10 characters max)");
            lType = s.getString(1,10);

            System.out.println("\nEnter the amount of this type of light bulb in stock: (integer)");
            lStock = s.getInt();

            tree.insert(new LightBulb(lCode,lType,lWatts,lStock));
            System.out.println("\ndo you want to add another light bulb type and stock? (1 if yes, 9 to return)");
            int exit = s.getInt();
            if (exit==1) generateLb();
    }

    private void report(){
        api.preOrderReport(tree);
    }
}
