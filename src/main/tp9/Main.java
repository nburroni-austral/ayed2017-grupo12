package main.tp9;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the text:\n");

        String text = scanner.nextLine();

        System.out.println("\n");

        SpellChecker spellChecker = new SpellChecker();

        spellChecker.checkForMisspellings(text);
    }
}
