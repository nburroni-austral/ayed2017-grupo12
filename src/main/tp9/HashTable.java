package main.tp9;

import java.util.List;

class HashTable {

    private HashEntry[] table;
    private int tableSize;
    Soundex soundex;

    HashTable(int tableSize) {
        this.tableSize = tableSize;
        this.table = new HashEntry[tableSize];
        for (int i = 0; i < tableSize; i++) {
            table[i] = null;
        }
        this.soundex = new Soundex();
    }

    List<String> get(String soundexCode) {
        int soundexCodeInt = soundexCode.hashCode();
        int hash = (soundexCodeInt % tableSize);
        if (table[hash] == null) return null;
        else {
            HashEntry entry = table[hash];
            for (int i = 0; i < entry.getValues().size(); i++) {
                if (entry.getSoundexCode().equals(soundexCode)) return entry.getValues();
            }
            return null;
        }
    }

    void put(String word) {
        String soundexCodeStr = soundex.getCode(word);
        int soundexCodeInt = soundexCodeStr.hashCode();
        int hash = (soundexCodeInt % tableSize);
        if (table[hash] == null) {
            table[hash] = new HashEntry(soundexCodeStr);
            table[hash].addValue(word);
        }
        else {
            HashEntry entry = table[hash];
            for (int i = 0; i < entry.getValues().size(); i++) {
                if (entry.getSoundexCode().equals(soundexCodeStr)) {
                    entry.addValue(word);
                    return;
                }
            }
        }
    }

    boolean containsSoundexCode(String soundexCode) {
        int soundexCodeInt = soundexCode.hashCode();
        int hash = (soundexCodeInt % tableSize);
        if (table[hash] != null) return true;
        return false;
    }

    boolean containsWord(String word) {
        int soundexCodeInt = soundex.getCode(word).hashCode();
        int hash = (soundexCodeInt % tableSize);
        if (table[hash] != null) {
            for (int i = 0; i < table[hash].getValues().size(); i++) {
                if (table[hash].getValues().get(i).equals(word)) return true;
            }
        }
        return false;
    }

    void removeSoundexCode(String soundexCode) {
        int soundexCodeInt = soundexCode.hashCode();
        int hash = (soundexCodeInt % tableSize);
        if (table[hash] != null) table[hash] = null;
    }

    void removeWord(String word) {
        int soundexCode = soundex.getCode(word).hashCode();
        int hash = (soundexCode % tableSize);
        if (table[hash] != null) {
            HashEntry entry = table[hash];
            for (int i = 0; i < entry.getValues().size(); i++) {
                if (entry.getValues().get(i).equals(word)) {
                    entry.getValues().remove(i);
                    return;
                }
            }
        }
    }

    void printTable() {
        for (int i = 0; i < table.length; i++) {
            if (table[i] != null && table[i].getValues().size() > 0) {
                System.out.println("SOUNDEX Code = " + table[i].getSoundexCode());
                System.out.print("Words: ");
                List<String> wordsList = table[i].getValues();
                for (int j = 0; j < wordsList.size(); j++) {
                    if (j == wordsList.size() - 1) System.out.print(wordsList.get(j) + ".");
                    else System.out.print(wordsList.get(j) + ", ");
                }
                System.out.println("\n");
            }
        }
    }
}
