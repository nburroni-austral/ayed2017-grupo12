package main.tp9;

import java.util.ArrayList;
import java.util.List;

class HashEntry {

    private String soundexCode;
    private List<String> wordsList;

    HashEntry(String soundexCode) {
        this.soundexCode = soundexCode;
        this.wordsList = new ArrayList<>();
    }

    void addValue(String value) {
        this.wordsList.add(value);
    }

    List<String> getValues() {
        return this.wordsList;
    }

    String getSoundexCode() {
        return this.soundexCode;
    }
}
