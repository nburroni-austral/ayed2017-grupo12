package main.tp9;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

class SpellChecker {

    private HashTable hashTable;

    SpellChecker() {
        String dictionary = "words_alpha.txt";
        hashTable = new HashTable(500000);
        try (BufferedReader br = new BufferedReader(new FileReader(dictionary))) {
            String sCurrentLine = br.readLine();
            while (sCurrentLine != null) {
                hashTable.put(sCurrentLine.toLowerCase().trim());
                sCurrentLine = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void checkForMisspellings(String text) {
        String theText = text.toLowerCase().replaceAll("[^\\w\\s]", " ");    // deletes non word chars
        String formatted = theText.trim().replaceAll(" +", " ");  // trim large whitespaces to just one
        String[] words = formatted.split("\\s");  // split the resulting string by its whitespaces
        boolean runOneTime = true;
        for (int i = 0; i < words.length; i++) {
            if (!hashTable.containsWord(words[i])) {
                if (runOneTime) System.out.println("Words Not Found:\n");
                runOneTime = false;
                String soundexCode = hashTable.soundex.getCode(words[i]);
                System.out.print("* \"" + words[i] + "\" : Did you mean... ");
                List<String> similarWords = hashTable.get(soundexCode);
                for (int j = 0; j < similarWords.size(); j++) {
                    if (j == similarWords.size() - 1) System.out.print(similarWords.get(j) + " ?");
                    else System.out.print(similarWords.get(j) + " / ");
                }
                System.out.println("\n");
            }
        }
    }
}
