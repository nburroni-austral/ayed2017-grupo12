package main.Sudoku.View;

import main.Sudoku.Board;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

public class MainWindow extends JFrame {
    private JButton solve = new JButton("Solve");
    private JButton changeCell = new JButton("Settings");
    private JFormattedTextField[][] boardNumbers = new JFormattedTextField[9][9];

    public MainWindow(int boardSize){

        super ("Sudoku");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);

        JPanel ButtonsPanel = new JPanel();
        ButtonsPanel.setLayout(new BoxLayout(ButtonsPanel,BoxLayout.LINE_AXIS));
        ButtonsPanel.add(Box.createHorizontalStrut(20));
        solve.setSize(200,100);
        ButtonsPanel.add(solve);
        ButtonsPanel.add(Box.createHorizontalStrut(20));
        ButtonsPanel.add(changeCell);
        ButtonsPanel.add(Box.createHorizontalStrut(20));
        ButtonsPanel.setVisible(true);

        JPanel sudokuPanel = new JPanel();
        sudokuPanel.setLayout(new GridLayout(9,9));
        //add textFields
        Dimension dimension = new Dimension(40,40);
        NumberFormatter nf = new NumberFormatter();
        nf.setFormat(NumberFormat.getInstance());
        nf.setMinimum(1);
        nf.setMaximum(9);
        for (int i = 0; i < boardSize;i++){
            for (int j = 0; j < boardSize;j++){
                JFormattedTextField numberField = new JFormattedTextField(nf);
                boardNumbers[i][j] = numberField;
                numberField.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                Font font = new Font("TimesNewPilarense", Font.BOLD,30);
                numberField.setHorizontalAlignment(JTextField.CENTER);
                numberField.setFont(font);
                numberField.setMinimumSize(dimension);
                numberField.setMaximumSize(dimension);
                numberField.setPreferredSize(dimension);
                sudokuPanel.add(numberField);
            }
        }
        sudokuPanel.setVisible(true);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.PAGE_AXIS));
        mainPanel.add(Box.createVerticalStrut(20));
        mainPanel.add(sudokuPanel);
        mainPanel.add(Box.createVerticalStrut(20));
        mainPanel.add(ButtonsPanel);
        mainPanel.add(Box.createVerticalStrut(20));
        mainPanel.setVisible(true);

        add(mainPanel);
        pack();
        setVisible(true);

    }

    /**
     * turns the window visible
     */
    public void showSelf(){
        setVisible(true);
    }

    /**
     * turns the window invisible
     */
    public void hideSelf(){
        setVisible(false);
    }

    /**
     * adds a listener to the Solve button
     * @param listener the listener to be added to the button
     */
    public void addSolveListener(ActionListener listener){
        solve.addActionListener(listener);
    }

    /**
     * adds a listener to the Solve button
     * @param listener the listener to be added to the button
     */
    public void addChangeCellListener(ActionListener listener){
        changeCell.addActionListener(listener);
    }

    /**
     * returns the board the user used on the window
     * @return the board, as an double array of JFormattedTextField
     */
    public JFormattedTextField[][] getWindowBoardNumbers() {
        return boardNumbers;
    }

    public void changeWindowNumber(int x, int y, int n){
        boardNumbers[y][x].setValue(n);
    }

    public void refresh(Board b) {
        for (int i = 0; i < b.size(); i++){
            for (int j = 0; j < b.size(); j++){
                if (b.getNumbers()[i][j] != 0) boardNumbers[i][j].setText("" + b.getNumbers()[i][j]);
                if (b.getNumbers()[i][j] == 0) boardNumbers[i][j].setText("");
            }
        }
    }

    public void reset() {
        for (int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++){
                boardNumbers[i][j].setText("");
            }
        }
    }
}
