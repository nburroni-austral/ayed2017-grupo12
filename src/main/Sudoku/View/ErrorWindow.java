package main.Sudoku.View;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionListener;

public class ErrorWindow extends JFrame{
        private JButton ok;

    public ErrorWindow() {
        super("agregar celda");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);

        ok = new JButton("ok");
        JPanel panel = new JPanel();
        JLabel error = new JLabel("Sudoku has no possible solution");
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(Box.createVerticalStrut(20));
        panel.add(error);
        panel.add(Box.createVerticalStrut(20));
        ok.setAlignmentX(Component.CENTER_ALIGNMENT);
        error.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(ok);
        panel.add(Box.createVerticalStrut(20));

        add(panel);
        pack();
    }

    /**
     * turns the window visible
     */
    public void showSelf(){
        setVisible(true);
    }

    /**
     * turns the window invisible
     */
    public void hideSelf(){
        setVisible(false);
    }

    /**
     * adds a listener to the ok button
     * @param listener
     */
    public void addOkListener(ActionListener listener){
        ok.addActionListener(listener);
    }
}
