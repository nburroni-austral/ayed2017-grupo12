package main.Sudoku.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class SettingsWindow extends JFrame {
    private Integer[] numbers = {1,2,3,4,5,6,7,8,9};
    private JComboBox<Integer> numberListX = new JComboBox<>(numbers);
    private JComboBox<Integer> numberListY = new JComboBox<>(numbers);
    private JComboBox<Integer> numberListN = new JComboBox<>(numbers);
    private JButton change = new JButton("Change");
    private JButton reset = new JButton("Reset the Board");

    public SettingsWindow(){
        super("add cell");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(new JLabel("select the X coordinate of the cell to change"));
        panel.add(numberListX);
        panel.add(Box.createVerticalStrut(20));
        panel.add(new JLabel("select the Y coordinate of the cell to change"));
        panel.add(numberListY);
        panel.add(Box.createVerticalStrut(20));
        panel.add(new JLabel("select the number you desire"));
        panel.add(numberListN);
        panel.add(Box.createVerticalStrut(20));
        panel.add(change);
        panel.add(Box.createVerticalStrut(20));
        panel.add(reset);
        panel.add(Box.createVerticalStrut(20));
        panel.setVisible(true);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.LINE_AXIS));
        mainPanel.add(Box.createHorizontalStrut(20));
        mainPanel.add(panel);
        mainPanel.add(Box.createHorizontalStrut(20));
        mainPanel.setVisible(true);


        add(mainPanel);
        pack();
        setVisible(false);

    }

    /**
     * turns the window visible
     */
    public void showSelf(){
        setVisible(true);
    }

    /**
     * turns the window invisible
     */
    public void hideSelf(){
        setVisible(false);
    }


    /**
     * adds a listener to the change button
     * @param listener the listener to be added to the button
     */
    public void addChangeListener(ActionListener listener){
        change.addActionListener(listener);
    }

    /**
     * adds a listener to the reset button
     * @param listener the listener to be added to the button
     */
    public void addResetListener(ActionListener listener){
        reset.addActionListener(listener);
    }


    /**
     * returns the value selected on the X ComboBox
     */
    public Integer getSelectedX(){
        return (Integer) numberListX.getSelectedItem();
    }

    /**
     * returns the value selected on the Y ComboBox
     */
    public Integer getSelectedY(){
        return (Integer) numberListY.getSelectedItem();
    }

    /**
     * returns the value selected on the N ComboBox
     */
    public Integer getSelectedN(){
        return (Integer) numberListN.getSelectedItem();
    }

}
