package main.Sudoku;

public class UnsolvableSudokuException extends RuntimeException{
    public UnsolvableSudokuException(){
        super("Sudoku has no possible resolution");
    }
}
