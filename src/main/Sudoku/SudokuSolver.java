package main.Sudoku;

import struct.impl.DynamicStack;

public class SudokuSolver {

    private DynamicStack<DynamicStack<DynamicStack<Integer>>> boardStack = new DynamicStack<>();
    private int i = 0;
    private int j = 0;
    private Board board;
    private boolean[][] userEntry;

    public SudokuSolver(Board board) {
        this.board = board;
        userEntry = userEntry(board);
        boardStack.push(new DynamicStack<>());
    }

    /**
     * adds one number if possible to the board given, following the sudoku rules
     * @param board the Sudoku board to be solved
     * @return the Sudoku board solved
     */
    public Board solve(Board board){
        Integer[][] numbers = board.getNumbers();
        if (i>=0 && i< board.size()){
            if (j>=0 && j< board.size()){
                if (j == boardStack.peek().size()) {
                    DynamicStack<Integer> rowStack = possibleEntries(i,j,numbers);
                    if (userEntry[i][j]){
                        boardStack.peek().push(rowStack);
                        j++;
                    }
                    //no possible entries for that cell in particular
                    else if (rowStack.isEmpty()){
                        j--;
                    }
                    else{
                        boardStack.peek().push(rowStack);
                        numbers[i][j] = rowStack.peek();
                        j++;
                    }
                }
                else{
                    boardStack.peek().peek().pop();
                    if (boardStack.peek().peek().isEmpty()) {
                        if (!userEntry[i][j])
                            numbers[i][j] = 0;
                        boardStack.peek().pop();
                        j--;
                    }
                    else{
                        numbers[i][j] = boardStack.peek().peek().peek();
                        j++;
                    }
                }
            }
            //everything gone right, advance to next row
            if (j>=board.size()){
                boardStack.push(new DynamicStack<>());
                i++;
                j = 0;
            }
            else if (j<0){
                j = numbers[i].length-1;
                i--;
                boardStack.pop();
            }
        }
        //have checked all possible entries, none is valid
        if (i < 0)
            throw new UnsolvableSudokuException();

        board.setNumbers(numbers);
        return board;
    }

    /**
     * analyzes a given cell in the board and returns the possible correct entries to that cell
     * @param i horizontal value of the cell
     * @param j vertical value of the cell
     * @return a DynamicStack with the possible entries to the cell given
     */
    public DynamicStack<Integer> possibleEntries(int i, int j, Integer[][] board){
        int[] possibleEntries = new int[10];
        for (int x = 1; x < 10;x++){
            possibleEntries[x] = 0;
        }

        // for horizontal entries
        for (int y = 0; y < board.length;y++){
            if (board[i][y] != 0 && j!=y){
                possibleEntries[board[i][y]] = 1;
            }
        }

        // for vertical entries
        for (int x = 0; x < 9;x++){
            if (board[x][j] != 0 && x != i)  {
                possibleEntries[board[x][j]] = 1;
            }
        }

        //for 3x3 squares
        int k = 0;
        int l = 0;

        if (i>=0 && i<=2) k = 0;
        else if (i>=3 && i<=5) k = 3;
        else k = 6;

        if (j>=0 && j<=2) {
            l = 0;
        } else if (j>=3 && j<=5) l  = 3;
        else l = 6;

        for (int x = k; x< k+3; x++){
            for (int y = l; y< l+3; y++){
                if (board[x][y] !=0 && x-k != i && y-l != j) {
                    possibleEntries[board[x][y]] = 1;
                }
            }
        }

        //turn the ones in the array to the possible value, and the not possible values to 0
        for (int x = 1; x < 10;x++){
            if (possibleEntries[x] == 0)
                possibleEntries[x] = x;
            else possibleEntries[x] = 0;
        }
        //modify possibleEntries array to be a stack with only the possible values
        DynamicStack<Integer> possibleEntriesStack = new DynamicStack<>();
        for (int x = 1; x < 10;x++){
            if (possibleEntries[x] != 0) possibleEntriesStack.push(possibleEntries[x]);
        }
        return possibleEntriesStack;
    }


    /**
     * creates a map where the number can't be changed because it's been added by the user
     * @param board the initial board, before being solved
     * @return the map, it has value true if the number in that position has been added by the user
     */
    private boolean[][] userEntry(Board board){
        Integer[][] numbers = board.getNumbers();
        boolean[][] result = new boolean[board.size()][board.size()];
        for (int i = 0; i < board.size(); i++){
            for (int j = 0; j < board.size(); j++){
                if(numbers[i][j] != 0) {
                    if (!isSolvable(i,j,numbers[i][j],numbers)){
                        throw new UnsolvableSudokuException();
                    }
                    result[i][j] = true;
                }
            }
        }
        return result;
    }

    private boolean isSolvable (int i, int j, Integer n, Integer[][] board){
        DynamicStack<Integer> possibles = possibleEntries(i,j,board);
        while(!possibles.isEmpty()){
            if (possibles.peek().equals(n)) return true;
            possibles.pop();
        }
        return false;
    }
}
