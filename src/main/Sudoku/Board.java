package main.Sudoku;

/**
 * Board class creates a Sudoku Board
 */
public class Board {
    private Integer[][] board;
    private int size;

    /**
     * creates a Sudoku Board with all initial values as 0
     */
    public Board(int size) {
        this.size = size;
        board = new Integer[size][size];
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                board[i][j] = 0;
            }
        }
    }

    /**
     * changes the value of a cell
     * @param x cell horizontal coordinate
     * @param y cell vertical coordinate
     * @param n new cell value
     */
    public void changeCell(int x, int y, Integer n){
        board[x][y] = n;
    }

    /**
     * checks if the board if full of numbers
     * @return true if it is, false if it's not
     */
    public boolean isFull(){
        for (int x = 0; x < size;x++){
            for (int y = 0; y < size;y++){
                if (board[x][y] == 0) return false;
            }
        }
        return true;
    }

    /**
     * returns the size of the board, indicated as how many rows/columns it has.
     * only returns the number of rows as it's the same number of columns.
     * @return the size of the board
     */
    public int size(){
        return size;
    }


    /**
     * prints the board on the console
     */
    public void printBoard(){

        for (int i = 0; i < size;i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * returns the numbers of the board
     * @return the numbers of the board in Integer[][] form
     */
    public Integer[][] getNumbers(){
        return board;
    }


    public void setNumbers(Integer[][] numbers) {
        this.board = numbers;
    }
}