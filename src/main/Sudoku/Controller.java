package main.Sudoku;

import main.Sudoku.View.ErrorWindow;
import main.Sudoku.View.MainWindow;
import main.Sudoku.View.SettingsWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller {
    private MainWindow mainWindow;
    private SettingsWindow settingsWindow;
    private ErrorWindow errorWindow;
    private SudokuSolver sudokuSolver;
    private Board board;
    Timer timer;


    public Controller(int boardSize){
        board = new Board(boardSize);
        errorWindow = new ErrorWindow();
        errorWindow.addOkListener(new okListener());
        mainWindow = new MainWindow(boardSize);
        mainWindow.addSolveListener(new initialSolveListener());
        mainWindow.addChangeCellListener(new changeMainListener());
        settingsWindow = new SettingsWindow();
        settingsWindow.addChangeListener(new changeListener());
        settingsWindow.addResetListener(new resetListener());
        solveListener solveListener = new solveListener();
        timer = new Timer(10,solveListener);
    }

    /**
     * class to add listeners to the change button, and implement it's behaviour when clicked
     */
    private class changeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int x = settingsWindow.getSelectedX();
            int y = settingsWindow.getSelectedY();
            int n = settingsWindow.getSelectedN();
            board.changeCell(x-1,y-1,n);
            mainWindow.changeWindowNumber(x-1,y-1,n);
            mainWindow.showSelf();
            settingsWindow.hideSelf();
        }
    }

    /**
     * class to add listeners to the Solve button in the main start, and implement it's behaviour when clicked
     */
    private class initialSolveListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            setBoardNumbers(mainWindow.getWindowBoardNumbers());
            try{
                sudokuSolver = new SudokuSolver(board);
                timer.start();
            }
            catch (UnsolvableSudokuException exception){
                errorWindow.showSelf();
            }
        }
    }

    private class solveListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try{
                board = sudokuSolver.solve(board);
            }
            catch (UnsolvableSudokuException exception){
                errorWindow.showSelf();
                timer.stop();
            }
            mainWindow.refresh(board);
        }

    }

    /**
     * class to add listeners to the change button on the change window, and implement it's behaviour when clicked
     */
    private class changeMainListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            mainWindow.hideSelf();
            settingsWindow.showSelf();
        }
    }

    /**
     * class to add listeners to the reset board button on the settings window, and implement it's behaviour when clicked
     */
    private class resetListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            mainWindow.reset();
            mainWindow.showSelf();
            settingsWindow.hideSelf();
        }
    }

    /**
     * class to add listeners to the ok button on the error popup, and implement it's behaviour when clicked
     */
    private class okListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            errorWindow.hideSelf();
        }
    }

    /**\
     * saves the numbers the user has entered on the board
     * @param b board in the Main window
     */
    private void setBoardNumbers(JFormattedTextField[][] b){
        for (int i = 0; i < b.length; i++){
            for (int j = 0; j < b.length; j++){
                if (!b[i][j].getText().equals("")){
                    board.changeCell(i,j,Integer.parseInt(b[i][j].getText()));
                }
            }
        }
    }
}